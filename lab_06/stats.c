#include "stats.h"

int get_value(FILE *const stream_in, FILE *const stream_out, int *const value)
{
    fprintf(stream_out, "Введите целое число:\n");

    if (fscanf(stream_in, "%d", value) == 1)
    {
        return OK;
    }
    
    return ERROR;
}

void set_default_timing_compares(stats_t *const stats)
{
    stats->compares_avl = 0;
    stats->compares_bst = 0;
    stats->compares_file = 0;
    stats->compares_hash = 0;
    stats->time_add_avl = 0;
    stats->time_add_bst = 0;
    stats->time_add_file = 0;
    stats->time_add_hash = 0;
}

void set_default_memory(stats_t *const stats)
{
    stats->memory_avl = 0;
    stats->memory_bst = 0;
    stats->memory_file = 0;
    stats->memory_hash_table = 0;
}

int add_to_file(const char *const filename, const int value, stats_t *const stats)
{
    FILE *stream = NULL;
    int value_from_file = 0, compare_times = 0;
    clock_t start = 0, end = 0;

    if ((stream = fopen(filename, "r")) == NULL)
    {
        errno = FILE_OPEN_ERROR;
        return ERROR;
    }

    while (fscanf(stream, "%d", &value_from_file) == 1)
    {
        compare_times++;

        if (value_from_file == value)
        {
            errno = DUPLICATE_VALUE;
            return ERROR;
        }
    }

    stats->compares_file = compare_times;
    fclose(stream);
    start = clock();

    if ((stream = fopen(filename, "a")) == NULL)
    {
        errno = FILE_OPEN_ERROR;
        return ERROR;
    }

    fprintf(stream, " %d", value);
    end = clock();
    stats->time_add_file = (double) (end - start) / CLOCKS_PER_SEC;
    fclose(stream);

    return OK;
}

static tree_node_t *add_node(tree_node_t *root, const int key, int *const compares)
{
    if (root == NULL)
    {   
        if ((root = malloc(sizeof(tree_node_t))) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
        else
        {
            root->left = NULL;
            root->right = NULL;
            root->value = key;
        }
    }       
    else if (root->value > key)
    {
        (*compares)++;

        if ((root->left = add_node(root->left, key, compares)) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
    }
    else if (root->value < key)
    {
        (*compares)++;

        if ((root->right = add_node(root->right, key, compares)) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
    }

    return root;
}

int add_to_bst(tree_node_t *root, const int value, stats_t *const stats)
{
    int compares = 0;
    clock_t start = 0, end = 0;

    start = clock();
    root = add_node(root, value, &compares);
    end = clock();

    stats->compares_bst = compares;
    stats->time_add_bst = (double) (end -start) / CLOCKS_PER_SEC;
    
    return OK;
}

int add_to_avl(tree_node_t *dsw_root, const int value, stats_t *const stats)
{
    int compares = 0;
    clock_t start = 0, end = 0;

    start = clock();
    dsw_root = add_node(dsw_root, value, &compares);
    end = clock();

    stats->compares_avl = compares;
    stats->time_add_avl = (double) (end -start) / CLOCKS_PER_SEC;
        
    return OK;
}

int add_to_hash(hash_table_t *hash, const int value, const int max_collisions, const int divisor, int (*hash_func)(const int, const int), stats_t *const stats)
{
    int pos = (*hash_func)(value, divisor);
    //printf("pos %d %d\n", pos, divisor);
    clock_t start = 0, end = 0;
    int check_times = 0, delta = 1, counter = 1;

    while (hash[pos].value != value)
    {
        //printf("cycle11\n");
        //printf("%d\n", hash[pos].value);
        //printf("pos- %d\n", pos);
        pos = (pos + delta) % divisor;
        delta = pow(++counter, 2);
    }

    hash[pos].flag = false;
    hash[pos].value = 0;

    delta = 1, counter = 1;
    
    //printf("here\n");

    start = clock();
    pos = (*hash_func)(value, divisor);

    while (hash[pos].flag)
    {
        //printf("dsds");
        check_times++;
        pos = (pos + delta) % divisor;
        delta = pow(++counter, 2);
    }

    hash[pos].value = value;
    //printf("%d - value_inside\n", hash[pos].value);
    hash[pos].flag = true;
    end = clock();
    stats->compares_hash = check_times;
    stats->time_add_hash = (double) (end -start) / CLOCKS_PER_SEC;

    return OK;       
}

int get_stats(stats_t *const stats,
               const char *const filename,
               tree_node_t *root, tree_node_t *dsw_root,
               hash_table_t *hash_table,
               const int divisor)
{
    FILE *stream = NULL;
    char symb = 0;
    int total_nodes = 1;
    size_t file_memory = 0, tree_memory = 0;

    if ((stream = fopen(filename, "r")) == NULL)
    {
        errno = FILE_OPEN_ERROR;
        return ERROR;
    }

    while (fscanf(stream , "%c", &symb) == 1)
    {
        file_memory++;
    }

    fclose(stream);
    stats->memory_file = file_memory;
    get_size(root, &total_nodes);
    tree_memory = sizeof(tree_node_t) * total_nodes;
    stats->memory_bst = tree_memory;
    stats->memory_avl = tree_memory;
    stats->memory_hash_table = divisor * sizeof(hash_table_t);  
    return OK;
}

void show_stats(FILE *const stream, stats_t *const stats)
{
    fprintf(stream, "Анализ времени добавления, кол-ва сравнений и затраченной памяти:\n\
1 - файл\n\
2 - ДБП\n\
3 - АВЛ-дерево\n\
4 - хэш-таблица\n\n");

    fprintf(stream, "Время добаления элемента(секунд)\n");
    fprintf(stream, "Файл - %lf\nДБП - %lf\nАВЛ-дерево - %lf\nхэш-таблица - %lf\n\n",
            stats->time_add_file,
            stats->time_add_bst,
            stats->time_add_avl,
            stats->time_add_hash);

    fprintf(stream, "Кол-во сравнений\n");
    fprintf(stream, "Файл - %d\nДБП - %d\nАВЛ-дерево - %d\nхэш-таблица - %d\n\n",
            stats->compares_file,
            stats->compares_bst,
            stats->compares_avl,
            stats->compares_hash);

    fprintf(stream, "Память(байт)\n");
    fprintf(stream, "Файл - %ld\nДБП - %ld\nАВЛ-дерево - %ld\nхэш-таблица - %ld\n\n",
            stats->memory_file,
            stats->memory_bst,
            stats->memory_avl,
            stats->memory_hash_table);
}