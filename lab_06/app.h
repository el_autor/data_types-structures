#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include "struct.h"
#include "errors.h"
#include "tree.h"
#include "hash.h"
#include "hash_funcs.h"
#include "stats.h"

#define FILENAME "./source.txt"

#define CREATE_TREE 1
#define SHOW_TREE 2
#define GET_BALANCED_TREE 3
#define SHOW_BALANCED_TREE 4
#define BUILD_HASH 5
#define SHOW_HASH 6
#define ADD_ELEM_TO_ALL 7
#define SHOW_ADD_STATS 8
#define SET_COLLISION_LIMIT 9
#define GRAPH_OUTLOOK 10
#define EXIT 11

#endif