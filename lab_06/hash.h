#ifndef _HASH_H_

#define _HASH_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include  <math.h>
#include "errors.h"
#include "struct.h"
#include "hash_funcs.h"

#define TABLE_MAX_SIZE 1000

hash_table_t *create_hash_table(const char *const filename, const int divisor, const int max_collisions);
void show_table(FILE *const stream, hash_table_t *const hash);
int add_to_table(hash_table_t *const hash, const int value, const int max_collisions, const int divisor, int (*hash_func)(const int, const int));

#endif