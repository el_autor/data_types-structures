#include "tree.h"

static tree_node_t *add_node(tree_node_t *root, const int key)
{
    if (root == NULL)
    {   
        if ((root = malloc(sizeof(tree_node_t))) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
        else
        {
            root->left = NULL;
            root->right = NULL;
            root->value = key;
        }
    }       
    else if (root->value > key)
    {
        if ((root->left = add_node(root->left, key)) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
    }
    else if (root->value < key)
    {
        if ((root->right = add_node(root->right, key)) == NULL)
        {
            errno = MEMORY_NOT_ALLOCED;
            return NULL;
        }
    }

    return root;
}

void free_tree(tree_node_t *root)
{   
    if (root)   
    {
        free_tree(root->left);
        free_tree(root->right);
        free(root);
    }
}

tree_node_t *input_tree(const char *const filename)
{
    tree_node_t *root = NULL;
    FILE *stream = NULL;
    int number;

    if ((stream = fopen(filename, "r")) == NULL)
    {
        errno = FILE_OPEN_ERROR;
        return NULL;
    }

    while (fscanf(stream, "%d", &number) == 1)
    {
        root = add_node(root, number);
    }
    
    fclose(stream);

    return root;
}

void show_tree(FILE *const stream, tree_node_t *const root, int level)
{
    if (root)
    {
        show_tree(stream, root->left, level + 1);

        for (int i = 0; i < level; i++)
        {
            fprintf(stream, "    ");
        }

        fprintf(stream, "%3d\n", root->value);
        show_tree(stream, root->right, level + 1);
    }    
}

static void tree_to_vine(tree_node_t *root)
{
    tree_node_t *tail = root;
    tree_node_t *rest = tail->right;

    while (rest)
    {
        if (rest->left == NULL)
        {   
            tail = rest;
            rest = rest->right;
        }
        else
        {
            tree_node_t *temp = rest->left;
            rest->left = temp->right;
            temp->right = rest;
            rest = temp;
            tail->right = temp;
        }
    }
}

static void compress(tree_node_t *root, int count)
{
    tree_node_t *scanner = root;

    for (int i = 1; i < count; i++)
    {
        tree_node_t *child = scanner->right;
        scanner->right = child->right;
        scanner = scanner->right;
        child->right = scanner->left;
        scanner->left = child; 
    }
}

static void vine_to_tree(tree_node_t *root, int size)
{
    int leaves = size + 1 - pow(2, log2(size + 1));
    compress(root, leaves);
    size -= leaves;
    while (size > 1)
    {
        compress(root, size / 2);
        size /= 2;
    }
}

void get_size(tree_node_t *root, int *const size)
{
    if (root)
    {
        get_size(root->left, size);
        (*size)++;
        get_size(root->right, size);
    }
}

tree_node_t *get_balanced(tree_node_t *root)
{
    tree_node_t *pseudo_root = malloc(sizeof(tree_node_t));
    int size = 1;

    get_size(root, &size);
    pseudo_root->right = root;
    tree_to_vine(pseudo_root);
    vine_to_tree(pseudo_root, size);
    tree_node_t *local = pseudo_root->right;
    free(pseudo_root);
    return local;
}

static void preorder(FILE *const stream, tree_node_t *root)
{
    if (root)
    {
        fprintf(stream, "%d ", root->value);
        preorder(stream, root->left);
        preorder(stream, root->right);
    }
}

static void inorder(FILE *const stream, tree_node_t *root)
{
    if (root)
    {
        inorder(stream, root->left);
        fprintf(stream, "%d ", root->value);
        inorder(stream, root->right);
    }
}

static void postorder(FILE *const stream, tree_node_t *root)
{
    if (root)
    {
        postorder(stream, root->left);
        postorder(stream, root->right);
        fprintf(stream, "%d ", root->value);
    }
}

void make_outlook(FILE *const stream, tree_node_t *root)
{
    fprintf(stream, "Прямой порядок\n");
    preorder(stream, root);
    fprintf(stream, "\n");
    fprintf(stream, "Центрированный порядок\n");
    inorder(stream, root);
    fprintf(stream, "\n");
    fprintf(stream, "Обратный порядок\n");
    postorder(stream, root);
    fprintf(stream, "\n");
}