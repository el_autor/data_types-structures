#ifndef _TREE_H_

#define _TREE_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "errors.h"
#include "struct.h"

#define STDOUT_WIDTH 200
#define MAX_SIZE 100

void free_tree(tree_node_t *root);
tree_node_t *input_tree(const char *const filename);
void show_tree(FILE *const stream, tree_node_t *const root, int level);
tree_node_t *get_balanced(tree_node_t *root);
void get_size(tree_node_t *root, int *const size);
void make_outlook(FILE *const stream, tree_node_t *root);

#endif