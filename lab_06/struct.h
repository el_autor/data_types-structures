#ifndef _STRUCT_H_

#define _STRUCT_H_

#include <stdbool.h>

struct tree_node_t
{
    int value;
    struct tree_node_t *left;
    struct tree_node_t *right;
};

typedef struct tree_node_t tree_node_t;

typedef struct
{   
    int value;
    bool flag;
} hash_table_t;

typedef struct
{
    double time_add_file;
    double time_add_bst;
    double time_add_avl;
    double time_add_hash;
    int compares_file;
    int compares_bst;
    int compares_avl;
    int compares_hash;
    size_t memory_file;
    size_t memory_bst;
    size_t memory_avl;
    size_t memory_hash_table;
} stats_t;

#endif