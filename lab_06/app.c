#include "app.h"

void greeting(FILE *const stream)
{
    fprintf(stream, "Программа для анализа структур данных: деревья и хэш-таблицы\n\
Данные - целые числа в файле\n\
Используется закрытое хэширование для устранение коллизий\n\
");
}

void show_menu(FILE *const stream)
{
    fprintf(stream, "Выберите код операции:\n\
1) Создать ДБП\n\
2) Показать ДБП\n\
3) Получить АВЛ-дерево\n\
4) Вывести АВЛ-дерево\n\
5) Построить хэш-таблицу\n\
6) Вывести хэш-таблицу\n\
7) Добавить число в ДБП, АВЛ-дерево, в хэш-таблицу и файл\n\
8) Сравнить время добавления, объем памяти и кол-во сравнений\n\
9) Задать порог кол-ва сравнений для хэш-таблицы\n\
10) Сделать обход графа 3 способами\n\
11) Выйти из программы\n");
}

int show_error(FILE *const stream, const int err_code)
{
    switch (err_code)
    {
        case WRONG_CODE_INPUT:
            fprintf(stream, "Неверный ввод кода!\n");
            break;

        case WRONG_CODE:
            fprintf(stream, "Кода операции не существует!\n");
            break;

        case NO_TREE_FOUNDED:
            fprintf(stream, "Дерево не введено!\n");
            break;

        case FILE_OPEN_ERROR:
            fprintf(stream, "Файл не найден!\n");
            break;

        case MEMORY_NOT_ALLOCED:
            fprintf(stream, "Память не может быть выделена!\n");
            break;

        case DUPLICATE_VALUE:
            fprintf(stream, "Дублирующиеся значения!\n");
            break;

        case TABLE_IS_FULL:
            fprintf(stream, "Хэш таблицв переполнена!\n");
            break;

        case NO_SUCH_FUNCTION:
            fprintf(stream, "Не удалось подобрать функцию с меньшим количеством сравнений!\n");
            break;
        
        case MAX_COMPARES_NOT_INPUTED:
            fprintf(stream, "Не задано максимальное кол-во сравнений!\n");
            break;

        case TABLE_NOT_FILLED:
            fprintf(stream, "Таблица не заполнена!\n");
            break;

        case WRONG_COMPARES_INPUT:
            fprintf(stream, "Неправильный ввод кол-ва сравнений!\n");
            break; 

        case NOT_ALL_INPUTED:
            fprintf(stream, "Не все структуры данных введены!\n");
            break;        

        case WRONG_VALUE_INPUT:
            fprintf(stream, "Неверно введено число!\n");
            break; 
    }

    return err_code;
}

int main()
{
    bool process = true;
    int code = 0;
    tree_node_t *root = NULL, *root_copy = NULL, *dsw_root = NULL;
    hash_table_t *hash_table = NULL;
    stats_t stats;
    clock_t start, end = 0;
    set_default_timing_compares(&stats);
    set_default_memory(&stats);
    int simple_divisors[] = { 11, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
    int max_compares = -1, value = 0, i = 0, divisor = 0;
    //double mid_add_tree = 0, mid_add_avl = 0, mid_add_hash = 0, mid_add_file = 0;

    greeting(stdout);

    while (process)
    {
        show_menu(stdout);

        if (fscanf(stdin, "%d", &code) != 1)
        {
            show_error(stdout, WRONG_CODE_INPUT);
        }

        switch (code)
        {
            case CREATE_TREE:
                if (root)
                {
                    free_tree(root);
                    root = NULL;
                }

                if ((root = input_tree(FILENAME)) == NULL)
                {  
                    show_error(stdout, errno);
                }

                break;
            case SHOW_TREE:
                if (root == NULL)
                {
                    show_error(stdout, NO_TREE_FOUNDED);
                }
                else
                {
                    show_tree(stdout, root, 0);
                }
                
                break;
            case GET_BALANCED_TREE:
                if (dsw_root)
                {
                    free_tree(dsw_root);
                    dsw_root = NULL;
                    root_copy = NULL;
                }

                if ((root_copy = input_tree(FILENAME)) == NULL)
                {
                    show_error(stdout, errno);
                }

                if ((dsw_root = get_balanced(root_copy)) == NULL)
                {
                    show_error(stdout, errno);
                }

                break;
            case SHOW_BALANCED_TREE:
                if (dsw_root == NULL)
                {
                    show_error(stdout, NO_TREE_FOUNDED);
                }
                else
                {
                    show_tree(stdout, dsw_root, 0);
                }
                
                break;
            case BUILD_HASH:
                if (hash_table != NULL)
                {   
                    free(hash_table);
                }

                if (max_compares == -1)
                {
                    show_error(stdout, MAX_COMPARES_NOT_INPUTED);
                }
                else
                {
                    i = 0;
                    divisor = simple_divisors[i];

                    while ((hash_table = create_hash_table(FILENAME, divisor, max_compares)) == NULL)
                    {
                        if (errno)
                        {
                            show_error(stdout, errno);
                            break;
                        }

                        divisor = simple_divisors[++i];

                        if (i > 19)
                        {
                            show_error(stdout, NO_SUCH_FUNCTION);
                            break;
                        }
                    }
                }                       

                break;
            case SHOW_HASH:
                if (hash_table == NULL)
                {
                    show_error(stdout, TABLE_NOT_FILLED);
                }
                else
                {
                    show_table(stdout, hash_table);
                } 

                break;
            case ADD_ELEM_TO_ALL:
                if (!root || !dsw_root || !hash_table)
                {
                    show_error(stdout, NOT_ALL_INPUTED);
                }
                else if (get_value(stdin, stdout, &value) == OK)
                {
                    set_default_timing_compares(&stats);

                    if (add_to_file(FILENAME, value, &stats) == OK)
                    {
                        add_to_bst(root, value, &stats);
                        add_to_avl(dsw_root, value, &stats);
                        start = clock();
                        dsw_root = get_balanced(dsw_root);
                        end = clock();
                        stats.time_add_avl += (double) (end - start) / CLOCKS_PER_SEC;
                        i = 0;
                        divisor = simple_divisors[i];

                        while ((hash_table = create_hash_table(FILENAME, divisor, max_compares)) == NULL)
                        {
                            if (errno)
                            {
                                show_error(stdout, errno);
                                break;
                            }

                            divisor = simple_divisors[++i];

                            if (i > 19)
                            {
                                show_error(stdout, NO_SUCH_FUNCTION);
                                break;
                            }
                        }

                        if (i <= 19)
                        {
                            add_to_hash(hash_table, value, max_compares, divisor, &mod_function, &stats);
                        }    
                    }
                    else
                    {
                        show_error(stdout, errno);
                    }
                }
                else
                {
                    show_error(stdout, WRONG_VALUE_INPUT);
                }  

                break;
            case SHOW_ADD_STATS:
                if (!root || !dsw_root || !hash_table)
                {
                    show_error(stdout, NOT_ALL_INPUTED);
                }
                else
                { 
                    set_default_memory(&stats);
                    get_stats(&stats, FILENAME, root, dsw_root, hash_table, divisor);
                    show_stats(stdout, &stats);
                }

                break;
            case SET_COLLISION_LIMIT:
                fprintf(stdout, "Введите максимальное число сравнений для хэш-таблицы:\n");
 
                if (fscanf(stdin, "%d", &max_compares) != 1 || max_compares < 1)
                {
                    show_error(stdout, WRONG_COMPARES_INPUT);
                }
                break;
            case GRAPH_OUTLOOK:
                if (!root)
                {
                    show_error(stdout, NO_TREE_FOUNDED);
                }
                else
                {
                    make_outlook(stdout, root);
                }

                break;
            case EXIT:
                process = false;
                free_tree(root);
                free_tree(dsw_root);
                free(hash_table);
                break;

            default:
                show_error(stdout, WRONG_CODE);
        }
    }

    return OK;
}