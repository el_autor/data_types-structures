#ifndef _STATS_H_

#define _STATS_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include "struct.h"
#include "errors.h"
#include "hash.h"
#include "tree.h"

int get_value(FILE *const stream_in, FILE *const stream_out, int *const value);
void set_default_timing_compares(stats_t *const stats);
void set_default_memory(stats_t *const stats);
int add_to_file(const char *const filename, const int value, stats_t *const stats);
int add_to_bst(tree_node_t *root, const int value, stats_t *const stats);
int add_to_avl(tree_node_t *dsw_root, const int value, stats_t *const stats);
void show_stats(FILE *const stream, stats_t *const stats);
int get_stats(stats_t *const stats,
               const char *const filename,
               tree_node_t *root, tree_node_t *dsw_root,
               hash_table_t *hash_table,
               const int divisor);
int add_to_hash(hash_table_t *hash, const int value, const int max_collisions, const int divisor, int (*hash_func)(const int, const int), stats_t *const stats);

#endif