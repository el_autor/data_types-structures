#include "hash_funcs.h"

int mod_function(const int key, const int divisor)
{
    return abs(key) % divisor;
}
