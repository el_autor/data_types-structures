#include "hash.h"

int add_to_table(hash_table_t *const hash, const int value, const int max_collisions, const int divisor, int (*hash_func)(const int, const int))
{
    int pos = (*hash_func)(value, divisor);
    //printf("divisor - %d\n", divisor);
    //printf("pos - %d\n", pos);
    int check_times = 0, delta = 1, counter = 1;
    //printf("%d -divisor\n", divisor);
    while (hash[pos].flag)
    {
        if (check_times == divisor - 1)
        {
            errno = TABLE_IS_FULL;
            return ERROR;
        }

        check_times++;
        pos = (pos + delta) % divisor;
        delta = pow(++counter, 2);

        if (check_times == max_collisions + 1)
        {
            //printf("colls - %d\n", value);
            return ERROR;
        }
    }

    hash[pos].value = value;
    //printf("%d - value_inside\n", hash[pos].value);
    hash[pos].flag = true;

    return OK;
}

static void make_default(hash_table_t *const hash, const int size)
{
    for (int i = 0; i < size; i++)
    {
        (hash + i)->flag = false;
        (hash + i)->value = 0;
    }
}

hash_table_t *create_hash_table(const char *const filename, const int divisor, const int max_collisions)
{
    FILE *stream = NULL;
    hash_table_t *hash_table = NULL;
    int value = 0;

    if ((hash_table = malloc(sizeof(hash_table_t) * TABLE_MAX_SIZE)) == NULL)
    {
        errno = MEMORY_NOT_ALLOCED;
        return NULL;
    }

    make_default(hash_table, TABLE_MAX_SIZE);

    if ((stream = fopen(filename, "r")) == NULL)
    {
        free(hash_table);
        errno = FILE_OPEN_ERROR;
        return NULL;
    }

    while (fscanf(stream, "%d", &value) == 1)
    {
        if (add_to_table(hash_table, value, max_collisions, divisor, &mod_function) != OK)
        {
            //printf("free\n");
            free(hash_table);
            return NULL;
        }
    }

    fclose(stream);

    return hash_table;
}

void show_table(FILE *const stream, hash_table_t *const hash)
{
    fprintf(stream, "Position | Value\n");

    for (int i = 0; i < TABLE_MAX_SIZE; i++)
    {
        if (hash[i].flag)
        {
            fprintf(stream, "%8d | %-d\n", i , hash[i].value);
        }
    }
}