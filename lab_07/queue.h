#ifndef _QUEUE_H_

#define _QUEUE_H_

#include <stdlib.h>
#include "struct.h"

typedef struct
{
    node_t *queue[MAX_NODES];
    node_t **head;
    node_t **tail;
} queue_t;

void add_node(queue_t *const queue, node_t *node);
void delete_node(queue_t *const queue);

#endif