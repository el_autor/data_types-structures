#ifndef _STRUCT_H_

#define _STRUCT_H_

#include <stdbool.h>

struct node
{
    int identifier;
    bool was_checked;
    struct node *next;
};

typedef struct node node_t;

#define MAX_NODES 100

#endif