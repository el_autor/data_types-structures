#ifndef _GRAPH_H

#define _GRAPH_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "struct.h"
#include "errors.h"
#include "queue.h"
#include "time.h"

node_t **graph_create(FILE *const stream_in, FILE *const stream_out);
int translate_to_dot(const char *const filename, node_t **graph);
void free_graph(node_t **graph);
int *get_unreachable(FILE *const stream, FILE *const stream_in, node_t **graph, double *const time);
size_t get_memory_outlay(node_t **graph);

#endif