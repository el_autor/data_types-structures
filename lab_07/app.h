#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <stdbool.h>
#include "errors.h"
#include "struct.h"
#include "graph.h"

#define FILENAME "graph.gv"

#define INPUT_GRAPH 1
#define SHOW_UNREACHABLE_NODES 2
#define GET_STATS 3
#define EXIT 4

#endif