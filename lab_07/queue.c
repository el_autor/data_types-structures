#include "queue.h"

void add_node(queue_t *const queue, node_t *node)
{
    *(queue->tail) = node;
    queue->tail--;
}

void delete_node(queue_t *const queue)
{
    for (node_t **i = queue->head; i > queue->tail + 1; i--)
    {
        *(i) = *(i - 1);
    }

    queue->tail++;
    *(queue->tail) = NULL;
}