#include "graph.h"

static void make_default(node_t **graph)
{
    for (int i = 0; i < MAX_NODES; i++)
    {
        graph[i] = NULL;
    }
}

static int get_node(FILE *const stream, node_t *head, const int node_number)
{
    node_t *node = NULL;
    int connect_node = 0;

    while (connect_node != -1)
    {
        fscanf(stream, "%d", &connect_node);

        if (connect_node == -1)
        {
            break;
        }

        if ((node = malloc(sizeof(node_t))) == NULL)
        {
            errno = MEMORY_ERROR;
            return ERROR;
        }
        else
        {
            node->identifier = connect_node;
            node->next = NULL;
            head->next = node;
            head = head->next;
        }
    }

    return OK;
}

node_t **graph_create(FILE *const stream_in, FILE *const stream_out)
{
    node_t **graph = NULL;
    int node_number = 0, counter = 0;

    if ((graph = malloc(sizeof(node_t *) * MAX_NODES)) == NULL)
    {
        errno = MEMORY_ERROR;
        return NULL;
    }

    make_default(graph);

    fprintf(stream_out, "Введите номер вершины и вершины в которые из нее возможен переход\n\
(конец ввода - номер вершины = -1, как для конца общего ввода, так и для ввода смежных вершин)\n");
    
    while (node_number != -1 && counter < 100)
    {
        if (fscanf(stdin, "%d", &node_number) != 1)
        {
            errno = WRONG_NODE_NUMBER_INPUT;
            free(graph);
            return NULL;
        }

        if (node_number == -1)
        {
            //printf("here\n");
            break;
        }

        //printf("node_numb - %d\n", node_number);

        graph[counter] = malloc(sizeof(node_t));
        graph[counter]->identifier = node_number;
        graph[counter]->next = NULL;

        if (get_node(stream_in, graph[counter], node_number) != OK)
        {
            free(graph);
            return NULL;
        }

        counter++;   
    }
    
    if (counter == MAX_NODES - 1)
    {
        errno = GRAPH_TOO_BIG;
        free(graph);
        return NULL;
    }

    return graph;
}

static void formatting(FILE *const stream,  const int cur_node, node_t *node)
{
    while (node)
    {
        fprintf(stream, "\t%d -> %d;\n", cur_node, node->identifier);
        node = node->next;
    }
}

int translate_to_dot(const char *const filename, node_t **graph)
{
    FILE *stream = NULL;
    int index = 0;

    if ((stream = fopen(filename, "w")) == NULL)
    {
        errno = FILE_NOT_OPEN;
        return ERROR;
    }

    fprintf(stream, "digraph my_graph\n{\n");
    
    while (graph[index])
    {   
        formatting(stream, graph[index]->identifier, graph[index]->next);
        index++;
    }

    fprintf(stream, "}\n");
    fclose(stream);

    return OK;
}

void free_graph(node_t **graph)
{
    node_t *local = NULL;

    for (int i = 0; i < MAX_NODES; i++)
    {
        while (graph[i])
        {
            local = graph[i]->next;
            free(graph[i]);
            graph[i] = local;
        }
    }

    free(graph);
}

static bool is_new(int *const unreachable_nodes, const int new_node, const int size)
{
    for (int i = 0; i < size; i++)
    {
        if (unreachable_nodes[i] == new_node)
        {
            return false;
        } 
    }

    return true;
}

static void not_reached(int *const unreachable_nodes, node_t **graph)
{
    int graph_index = 0, fill_index = 0;
    node_t *local = NULL; 

    while (graph[graph_index])
    {
        local = graph[graph_index];

        while (local)
        {
            if (is_new(unreachable_nodes, local->identifier, fill_index))
            {
                unreachable_nodes[fill_index] = local->identifier;
                fill_index++;    
            }

            local = local->next;
        }

        graph_index++;
    }
}

static void set_not_exist(int *const unreachable_nodes)
{
    for (int i = 0; i < MAX_NODES; i++)
    {
        unreachable_nodes[i] = -1;
    }
}

static void set_unchecked(node_t **graph)
{
    int index = 0;
    node_t *local = NULL;

    while (graph[index])
    {   
        local = graph[index];

        while (local)
        {
            local->was_checked = false;
            local = local->next;        
        }

        index++;
    }
}

static node_t *pull_node(node_t **graph, const int start_number)
{
    int index = 0;
    //printf("%d - number\n", start_number);

    while (graph[index])
    {
        //printf("%d ident\n", graph[index]->identifier);
        if (graph[index]->identifier == start_number)
        {
            return graph[index];
        }

        index++;
    }

    return NULL;
}

static void delete_from_unreachable(int *const unreachable_nodes, const int identifier)
{
    //printf("%d id\n", identifier);
    for (int i = 0; i < MAX_NODES; i++)
    {
        //printf("%d index\n", i);
        //printf("%d val\n", unreachable_nodes[i]);
        if (unreachable_nodes[i] == identifier)
        {
            unreachable_nodes[i] = -1;
        }
    }
}

int *get_unreachable(FILE *const stream_out, FILE *const stream_in, node_t **graph, double *const time)
{
    int *unreachable_nodes = NULL, start_number = 0;
    queue_t node_queue;
    node_queue.tail = node_queue.head = node_queue.queue + MAX_NODES - 1;
    node_t *start_node = NULL;
    clock_t start, end = 0;
    
    for (int i = 0; i < MAX_NODES; i++)
    {
        node_queue.queue[i] = NULL;
    }

    if ((unreachable_nodes = malloc(sizeof(int) * MAX_NODES)) == NULL)
    {
        errno = MEMORY_ERROR;
        return NULL;
    }

    start = clock();

    set_not_exist(unreachable_nodes);
    not_reached(unreachable_nodes, graph);
    set_unchecked(graph);
    fprintf(stream_out, "Введите номер вершины\n");
    fscanf(stream_in, "%d", &start_number);

    if ((start_node = pull_node(graph, start_number)) == NULL)
    {
        delete_from_unreachable(unreachable_nodes, start_number);
        end = clock();
        *time = (double) (end - start) / CLOCKS_PER_SEC;
        return unreachable_nodes;
    }

    add_node(&node_queue, start_node);

    while (node_queue.head != node_queue.tail)
    {
        //printf("%ld - queue_size\n", node_queue.head - node_queue.tail);
        //printf("here1\n");
        (*node_queue.head)->was_checked = true;
        //printf("pointer - %p\n", unreachable_nodes);
        delete_from_unreachable(unreachable_nodes, (*node_queue.head)->identifier);
        //printf("head ident - %d\n", (*node_queue.head)->identifier);
        node_t *connect_node = (*node_queue.head), *local = NULL;

        while (connect_node)
        {          
            //printf("here2\n");
            if ((local = pull_node(graph, connect_node->identifier)) && !local->was_checked)
            {
                while (local)
                {
                    add_node(&node_queue, local);
                    local = local->next;
                }
            }
            else if (!connect_node->was_checked)
            {
                add_node(&node_queue, connect_node);
            }

            connect_node = connect_node->next;
        }

        delete_node(&node_queue); 
    }

    end = clock();
    *time = (double) (end - start) / CLOCKS_PER_SEC;
    return unreachable_nodes;
}

size_t get_memory_outlay(node_t **graph)
{
    node_t *local = NULL;
    size_t total_memory = 0;
    int index = 0;

    while (graph[index])
    {
        local = graph[index];
        total_memory += sizeof(node_t *);

        while (local)
        {
            total_memory += sizeof(node_t);
            local = local->next;
        }

        index++;
    }

    return total_memory;
}