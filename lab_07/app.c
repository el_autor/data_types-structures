#include "app.h"

void greeting(FILE *const stream)
{
    fprintf(stream, "Программа для нахождения всех недостижимых вершин орграфа из заданной вершины\n");
}

void show_menu(FILE *const stream)
{
    fprintf(stream, "Выберите опцию:\n\
1) Ввести орграф\n\
2) Найти недостижимые вершины из данной\n\
3) Получить затраты времени и памяти\n\
4) Выйти их программы\n");
}

int show_error(FILE *const stream, const int err_code)
{
    switch (err_code)
    {
        case WRONG_OPTION_INPUT:
            fprintf(stream, "Неправильный ввод опции\n");
            break;

        case WRONG_OPTION_VALUE:
            fprintf(stream, "Такой опции не существует\n");
            break;
    }

    return err_code;
}

void show_unreachable(FILE *const stream, int *const nodes)
{
    fprintf(stream, "Номера недостижимых вершин:\n");
    bool existence = false;

    for (int i = 0; i < MAX_NODES; i++)
    {
        if (nodes[i] != -1)
        {
            existence = true;
            fprintf(stream, "%d ", nodes[i]);
        }
    }

    if (!existence)
    {
        fprintf(stream, "Все вершины достижимы\n");
    }

    fprintf(stream, "\n");
}

int main()
{
    bool process = true;
    int option_code = 0;
    greeting(stdout);
    node_t **graph = NULL;
    int *unreachable_nodes = NULL;
    double time = 0;
    //size_t memory = 0;

    while (process)
    {
        show_menu(stdout);

        if (fscanf(stdin, "%d", &option_code) != 1)
        {
            show_error(stdout, WRONG_OPTION_INPUT);
        }

        switch (option_code)
        {
            case INPUT_GRAPH:
                if (graph)
                {
                    free_graph(graph);
                }

                if ((graph = graph_create(stdin, stdout)) == NULL)
                {
                    show_error(stdout, errno);
                }
                else
                {
                    //printf("ouuf\n");
                    if (translate_to_dot(FILENAME, graph) != OK)
                    {
                        show_error(stdout, errno);
                    }
                }

                break;
            case SHOW_UNREACHABLE_NODES:
                if (unreachable_nodes)
                {
                    free(unreachable_nodes);
                    unreachable_nodes = NULL;
                }

                if (graph == NULL)
                {
                    show_error(stdout, GRAPH_NOT_EXIST);
                }
                else if ((unreachable_nodes = get_unreachable(stdout, stdin, graph, &time)) == NULL)
                {
                    show_error(stdout, errno);
                }

                if (unreachable_nodes)
                {
                    show_unreachable(stdout, unreachable_nodes);
                }

                break;
            case GET_STATS:
                if (graph && unreachable_nodes)
                {
                    fprintf(stdout, "Время - %lf(сек)\n", time);
                    fprintf(stdout, "Память - %ld(байт)\n", get_memory_outlay(graph));
                }
                else
                {
                    show_error(stdout, NODES_NOT_CALCULATED);
                }

                break;
            case EXIT:
                process = false;

                if (graph)
                { 
                   free_graph(graph);
                }

                free(unreachable_nodes);
                break;

            default:
                show_error(stderr, WRONG_OPTION_VALUE);
        }
    }

    return OK;
}