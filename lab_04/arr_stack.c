#include "arr_stack.h"

int add_array(stack_array_t *const stack, size_t *value)
{
    int *pointer = NULL;

    if (stack->head == stack->end)
    {
        errno = STACK_FILLED;
        return ERROR;
    }

    if (value == NULL)
    {
        if (scanf("%p", &pointer) != 1)
        {
            errno = WRONG_POINTER_FORMAT;
            return ERROR;
        }
    }
    else
    {
        pointer = (int *) value;
    }

    (stack->head)++;
    *(stack->head) = pointer;
    return OK;
}

int delete_array(stack_array_t *const stack)
{
    if (stack->head == stack->start - 1)
    {
        errno = STACK_EMPTY;
        return ERROR;
    }

    (stack->head)--;

    return OK;
}

int check_array_last(stack_array_t *const stack)
{
    if (stack->head == stack->start - 1)
    {
        errno = STACK_EMPTY;
        return ERROR;
    }

    printf("%p\n", *(stack->head));

    return OK;
}

int check_array_all(FILE *const stream, stack_array_t *const stack)
{
    if (stack->head == stack->start - 1)
    {
        errno = STACK_EMPTY;
        return ERROR;
    }

    fprintf(stream, "Голова стека:\n");

    for (int **p = stack->head; p >= stack->start; p--)
    {
        fprintf(stream, "%p\n", *p);
    }

    return OK;
}