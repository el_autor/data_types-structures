#ifndef _EFFICIENCY_H_

#define _EFFICIENCY_H_

#include <time.h>
#include <errno.h>
#include "structs.h"
#include "arr_stack.h"
#include "list_stack.h"
#include "errors.h"

int get_stats();

#endif