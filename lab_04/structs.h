#ifndef _STRUCTS_H_

#define _STRUCTS_H_

#define STACK_SIZE 1000

typedef struct
{
    int **start;
    int **head;
    int **end;
} stack_array_t;

struct stack_list_t
{
    size_t *value;
    struct stack_list_t *next;
};

typedef struct stack_list_t stack_list_t;

#endif