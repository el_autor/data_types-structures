#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include "structs.h"
#include "errors.h"
#include "arr_stack.h"
#include "list_stack.h"
#include "efficiency.h"

#define START_UP true
#define STOP false

#define ADD_ARRAY 1
#define DELETE_ARRAY 2
#define CHECK_ARRAY_LAST 3
#define CHECK_ARRAY_ALL 4
#define ADD_LIST 5
#define DELETE_LIST 6
#define CHECK_LIST_LAST 7 
#define CHECK_LIST_ALL 8
#define SHOW_FREED_MEM 9
#define SHOW_TIME 10
#define EXIT 11

#endif