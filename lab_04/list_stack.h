#ifndef _LIST_STACK_H_

#define _LIST_STACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "structs.h"
#include "errors.h"

int add_list(stack_list_t **node, size_t *memory);
size_t *delete_list(stack_list_t **node);
int check_list_last(stack_list_t *node);
int check_list_all(FILE *const stream, stack_list_t *node);
int show_memory(stack_list_t *head);
void free_stack(stack_list_t *head);

#endif