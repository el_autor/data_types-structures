#include "list_stack.h"

int add_list(stack_list_t **head, size_t *memory)
{
    stack_list_t *local = malloc(sizeof(stack_list_t));

    if (!local)
    {
        errno = MEM_NOT_ALLOCED;
        return ERROR;
    }

    local->next = *head; // на первой итерации это и есть null

    if (memory == NULL)
    {
        local->value = (size_t *) &(local->value);
    }
    else
    {
        local->value = memory;
    }
    
    *head = local;

    return OK;
}

size_t *delete_list(stack_list_t **head)
{
    size_t *del_memory = NULL; 

    if (*head == NULL)
    {
        errno = STACK_EMPTY;
        return NULL;
    }    

    del_memory = (*head)->value;
    stack_list_t *local = *head;
    *head = (*head)->next;
    free(local);    
    return del_memory;
}

int check_list_last(stack_list_t *head)
{
    if (!head)
    {
        errno = STACK_EMPTY;
        return ERROR;
    }

    printf("%p\n", head->value);
    return OK;
}

int check_list_all(FILE *const stream, stack_list_t *head)
{
    if (head == NULL)
    {
        errno = STACK_EMPTY;
        return ERROR;
    }

    fprintf(stream, "Голова стека:\n");

    while (head)
    {
        fprintf(stream, "%p\n", head->value);
        head = head->next;
    }    

    return OK;
}

void free_stack(stack_list_t *head)
{
    stack_list_t *local = NULL;

    while (head)
    {
        local = head->next;
        free(head);
        head = local;
    }
}

int show_memory(stack_list_t *head)
{
    printf("Освобожденная память элементов стека:\n");

    if (head == NULL)
    {
        errno = NO_FREED_DATA;
        return ERROR;
    }

    printf("От последнего освобожденного адреса к первому:\n");

    while (head)
    {
        printf("%p\n", head->value);
        head = head->next;
    }    

    return OK;
}