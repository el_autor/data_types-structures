#include "efficiency.h"

int get_stats()
{
    // Хранение в виде статического массива
    int *array[STACK_SIZE];
    stack_array_t arr_stack;
    arr_stack.head = array - 1;
    arr_stack.start = array;
    arr_stack.end = array + STACK_SIZE - 1;

    // Хранение в виде списка
    stack_list_t *list_stack = NULL;
    clock_t start_time = 0, finish_time = 0;

    int elems_total = 0;
    printf("Введите количество элементов в стеке(не более 1000):\n");

    if (scanf("%d", &elems_total) != 1 || elems_total < 1 || elems_total > 1000)
    {
        errno = WRONG_ELEMS_NUMBER;
        return ERROR;
    }

    start_time = clock();

    for (size_t i = 0; i < elems_total; i++)
    {
        size_t *p = (size_t *) (i + 1);
        add_array(&arr_stack, p);
    }

    finish_time = clock(); 
    printf("Время на добавление в статический массив - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);
    
    start_time = clock();

    for (int i = 0; i < elems_total; i++)
    {
        add_list(&list_stack, NULL);   
    }

    finish_time = clock();
    printf("Время на добавление в список - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);

    /*
    FILE *output = fopen("out.txt", "w");
    
    start_time = clock();
    check_array_all(output, &arr_stack);
    finish_time = clock();
    printf("Время на просмотр статического массива - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);

    start_time = clock();
    check_list_all(output, list_stack);
    finish_time = clock();
    printf("Время на просмотр списка - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);

    fclose(output);
    */

    start_time = clock();

    for (int i = 0; i < elems_total; i++)
    {
        delete_array(&arr_stack);
    }

    finish_time = clock();
    printf("Время на очищение статического массива - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);

    start_time = clock();

    for (int i = 0; i < elems_total; i++)
    {
        delete_list(&list_stack);
    }

    finish_time = clock();
    printf("Время на очищение списка - %lf\n", (double) (finish_time - start_time) / CLOCKS_PER_SEC);    

    printf("Использование памяти:\n");
    printf("Статический массив - %ld\n", sizeof(int *) * elems_total);
    printf("Список - %ld\n", sizeof(stack_list_t) * elems_total);


    free_stack(list_stack);
    return OK;
}