#ifndef _ARR_STACK_H_

#define _ARR_STACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "structs.h"
#include "errors.h"

int add_array(stack_array_t *const stack, size_t *value);
int delete_array(stack_array_t *const stack);
int check_array_last(stack_array_t *const stack);
int check_array_all(FILE *const stream, stack_array_t *const stack);

#endif