#include "app.h"

void greeting()
{
    printf("Программа для работы со стеком с реализациями в виде статического массива(1000 эл.) и односвязного списка.\n\
Осуществляет добавление в стек, удаление из стека, и текущего состояния стека.\n\n");
}

void show_menu()
{
    printf("Выберите действие:\n\
    1 - добавить в стек(статический массив)\n\
    2 - удалить из стека(статический массив)\n\
    3 - посмотреть последний элемент(статический массив)\n\
    4 - посмотреть весь стек(статический массив)\n\
    5 - добавить в стек(список)\n\
    6 - удалить из стека(список)\n\
    7 - посмотреть последний элемент(список)\n\
    8 - посмотреть весь стек(список)\n\
    9 - вывести адреса освобожденной памяти\n\
    10 - вывести время работы реализации(добавление, удаление)\n\
    11 - выйти из программы\n");
}

int show_error(const int error_code)
{
    switch (error_code)
    {
        case WRONG_INPUT_CODE:
            printf("Ошибочный ввод кода!\n");
            break;
        
        case STACK_FILLED:
            printf("Стек переполнен!\n");
            break;

        case STACK_EMPTY:
            printf("Стек пуст!\n");
            break;        
        
        case WRONG_POINTER_FORMAT:
            printf("Неправильно введен указатель!\n");
            break;

        case MEM_NOT_ALLOCED:
            printf("Память не выделилась!\n");
            break; 

        case NO_FREED_DATA:
            printf("Память еще не освобождалась!\n");
            break;

        case WRONG_ELEMS_NUMBER:
            printf("Неверное количество элементов\n");
            break;
    }

    return error_code;
}

void clean_stdin()
{
    char symb = 0;
    while ((symb = getchar()) != '\n');
}

int main()
{
    // Хранение в виде статического массива
    int *array[STACK_SIZE];
    stack_array_t arr_stack;
    arr_stack.head = array - 1;
    arr_stack.start = array;
    arr_stack.end = array + STACK_SIZE - 1;

    // Хранение в виде списка
    stack_list_t *list_stack = NULL;
    stack_list_t *freed_list = NULL;
    size_t *memory = NULL;

    bool process = START_UP;
    int error_code = OK, code = 0;
    greeting();

    while (process)
    {
        show_menu();

        if (scanf("%d", &code) != 1 || code < 1 || code > 11)
        {
            show_error(WRONG_INPUT_CODE);
            clean_stdin();
            code = 0;
        }

        switch (code)
        {
            case ADD_ARRAY:
                if ((error_code = add_array(&arr_stack, NULL)))
                {
                    show_error(errno);

                    if (errno == WRONG_POINTER_FORMAT)
                    {
                        clean_stdin();
                    }
                }

                break;

            case DELETE_ARRAY:
                if ((error_code = delete_array(&arr_stack)))
                {
                    show_error(errno);
                }

                break;

            case CHECK_ARRAY_LAST:
                if ((error_code = check_array_last(&arr_stack)))
                {
                    show_error(errno);
                }

                break; 

            case CHECK_ARRAY_ALL:
                if ((error_code = check_array_all(stdout, &arr_stack)))
                {
                    show_error(errno);
                }

                break;

            case ADD_LIST:
                if ((error_code = add_list(&list_stack, NULL)))
                {
                    show_error(errno);

                    if (errno == WRONG_POINTER_FORMAT)
                    {
                        clean_stdin();
                    }
                }
                
                break;

            case DELETE_LIST:
                if ((memory = (delete_list(&list_stack))) == NULL)
                {
                    show_error(errno);
                }
                else
                {
                    add_list(&freed_list, memory);
                }

                break;

            case CHECK_LIST_LAST:
                if ((error_code = check_list_last(list_stack)))
                {
                    show_error(errno);
                }

                break; 

            case CHECK_LIST_ALL:
                if ((error_code = check_list_all(stdout, list_stack)))
                {
                    show_error(errno);
                }

                break; 

            case SHOW_FREED_MEM:
                if ((error_code = show_memory(freed_list)))
                {
                    show_error(errno);
                }

                break;

            case SHOW_TIME:
                if (get_stats())
                {
                    show_error(errno);
                }

                break;

            case EXIT:
                process = STOP;
                printf("Всего доброго!\n");
                break;
        }
    }

    free_stack(list_stack);
    free_stack(freed_list);
    return OK;
}