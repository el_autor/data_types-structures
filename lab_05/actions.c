#include "actions.h"

//int sum = 0;
//int times = 0;

double set_time(float down_limit, float up_limit)
{
    //printf("%f - %f\n", down_limit, up_limit);
    //printf("%f %f\n", down_limit, up_limit);
    //float k = drand48(); 
    double k = (double) (rand() % 1000000) / 1000001;
    //printf("%f\n", k);
    //times++;
    //sum += (int) round(1000000 * (up_limit - down_limit) * k + down_limit);
    //printf("%f\n", (up_limit - down_limit) * k + down_limit);
    //printf("%f\n", (1000000 * (up_limit - down_limit) * k + down_limit));
    //printf("%d-int\n", (int) round(1000000 * (up_limit - down_limit) * k + down_limit));
    return ((up_limit - down_limit) * k + down_limit);
}

// Увеличение ожидания в очереди на единицу
void plus_timing(array_queue_t *const timing)
{
    for (int *p = timing->tail + 1; p <= timing->head; p++)
    {
        (*p)++;
    }
}

int do_modeling_array(array_queue_t *const queue, array_queue_t *const timing, range_t *const range,\
                      info_t *const info, int *const max_size_state,  clock_t *const ticks_time)
{
    int recycled_1_type = 0, info_flag = 0, total_waiting_sum = 0;
    int time_arrival = 0, time_cycling = 0;
    double time_since_start = 0.000000, total_downtime = 0.000000;
    int size = 0;
    clock_t start_operation = 0, end_operation = 0;

    // Статистика
    int middle_length_sum = 0, delete_counter = 0, in_number = 0, in_1_type = 0, out_1_type = 0, in_2_type = 0;

    time_arrival = (int) round(1000000 * set_time((double) range->down_arrival_1 / 1000000, (double) range->up_arrival_1 / 1000000));
    //printf("%d - time_arrival\n", time_arrival);
    //time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_2 / 1000000, (double) range->up_cycling_2 / 1000000));
    //printf("%d - time_cycling\n", time_cycling);
    start_operation = clock();
    add_to_tail_array(queue, TYPE_2);
    end_operation = clock();
    (*ticks_time) += end_operation - start_operation;
    add_to_tail_array(timing, 0);
    in_number++;
    in_2_type++;

    while (recycled_1_type < TOTAL_CYCLED)
    {
        size = queue->head - queue->tail;

        if (size > *max_size_state)
        {
            *max_size_state = size;
        }
        /*
        if (recycled_1_type <= 100)
        {
            printf("here %d - time_arr, %d - time_cycl\n", time_arrival, time_cycling);
            for (int *p = queue->tail + 1; p <= queue->head; p++)
            {
                //printf("here ");
                printf("%-4d ", *p);
            }
            printf("\n");
            
            for (int *p = timing->tail + 1; p <= timing->head; p++)
            {
                //printf("here ");
                printf("%-4d ", *p);
            }
            printf("\n");

            printf("%d - in_1_type\n", in_1_type);
            printf("%d - out_1_type\n", out_1_type);
            printf("%d - total_wait\n\n", total_waiting_sum);
        }
        */

        if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
        {
            info_flag = recycled_1_type;
            info->current_length = queue->head - queue->tail;
            info->length_mid = middle_length_sum / (float) delete_counter; 
            info->total_in_requests = in_number;
            info->total_out_requests = delete_counter;
            info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
            fprintf(stdout, "Всего обработано заявок - %d\n", recycled_1_type);   
            show_current_results(stdout, info);
            //printf("%d - in_1_type\n", in_1_type);
            //printf("%d - out_1_type\n\n", out_1_type);
        }

        time_since_start += 0.000001;
        //printf("%d\n", recycled_1_type);
        while (time_arrival == 0)
        {
            time_arrival = (int) round(1000000 * set_time((double) range->down_arrival_1 / 1000000, (double) range->up_arrival_1 / 1000000));
            //printf("%d - time_arrival\n", time_arrival);
            start_operation = clock();

            if (add_to_tail_array(queue, TYPE_1) != OK)
            {
                return ERROR;
            }

            end_operation = clock();
            (*ticks_time) += end_operation - start_operation;

            if (add_to_tail_array(timing, 0) != OK)
            {
                return ERROR;
            }

            in_number++;
            in_1_type++;
        }

        //printf("%f - time_arrival\n", time_arrival);
        time_arrival -= 1;

        while (time_cycling == 0 && queue->head != queue->tail)
        {
            if (recycled_1_type >= TOTAL_CYCLED)
            {
                break;
            }

            middle_length_sum += queue->head - queue->tail;
            delete_counter++;
            //printf("%d - time_cycling\n", time_cycling);

            if (*(queue->head) == TYPE_1)
            {
                time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_1 / 1000000, (double) range->up_cycling_1 / 1000000));
                recycled_1_type++;
            }

            if (*(queue->head) == TYPE_2)
            {
                time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_2 / 1000000, (double) range->up_cycling_2 / 1000000));
                in_2_type++;
                in_number++;
            }
            else
            {
                out_1_type++;
            }

            // Код связанный с подсчетом среднего времени --------------------
            total_waiting_sum += *(timing->head);
            //printf("%d - время в очереди\n", *(timing->head));

            for (int *p = timing->head; p > timing->tail + 1; p--)
            {
                *(p) = *(p - 1);
            }

            (timing->tail)++;

            if (*(queue->head) == TYPE_2)
            {
                if (timing->head - (timing->tail + 1) < 4)
                {
                    add_to_tail_array(timing, 0);
                }
                else
                {
                    for (int *p = (timing->tail + 1) - 1; p < (timing->head) - 4; p++)
                    {
                        *(p) = *(p + 1);
                    }

                    *((timing->head) - 4) = 0;
                    (timing->tail)--;
                }
            }
            // ---------------------------------------------------------------
            start_operation = clock();

            if (remove_from_head_array(queue) != OK)
            {
                return ERROR;
            }

            end_operation = clock();
            (*ticks_time) += end_operation - start_operation;

            if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
            {
                info_flag = recycled_1_type;
                info->current_length = queue->head - queue->tail;
                info->length_mid = middle_length_sum / (float) delete_counter; 
                info->total_in_requests = in_number;
                info->total_out_requests = delete_counter;
                info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
                fprintf(stdout, "Всего обработано заявок 1 типа - %d\n", recycled_1_type);   
                show_current_results(stdout, info);
            }       
        }

        time_cycling -= 1;

        if (time_cycling < 0)
        {
            total_downtime += 0.000001;
        }

        plus_timing(timing);
    }

    if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
    {
        info_flag = recycled_1_type;
        info->current_length = queue->head - queue->tail;
        info->length_mid = middle_length_sum / (float) delete_counter; 
        info->total_in_requests = in_number;
        info->total_out_requests = delete_counter;
        info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
        fprintf(stdout, "Всего обработано заявок 1 типа - %d\n", recycled_1_type);   
        show_current_results(stdout, info);
        //printf("%d - in_1_type\n", in_1_type);
        //printf("%d - out_1_type\n\n", out_1_type);
    }

    info->modelling_time = time_since_start;
    info->machine_downtime = total_downtime;
    info->total_in_1_type = in_1_type;
    info->total_out_1_type = out_1_type;
    info->total_in_type_2 = in_2_type; 
    //printf("%f - middle\n", sum / (float) times);
    return OK;
}

int input_ranges(FILE *const in_stream, FILE *const out_stream, range_t *const data)
{
    fprintf(out_stream, "Введите нижнюю и верхнюю границу времени обработки заявок 1 типа(в е.в, натуральные числа либо 0):\n");
    
    if (fscanf(in_stream, "%d %d", &(data->down_cycling_1), &(data->up_cycling_1)) != 2 || data->down_cycling_1 >= data->up_cycling_1 || data->down_cycling_1 < 0)
    {
        errno = WRONG_LIMITS;
        return ERROR;
    }

    fprintf(out_stream, "Введите нижнюю и верхнюю границу времени прибытия заявок 1 типа(в е.в, натуральные числа либо 0):\n");

    if (fscanf(in_stream, "%d %d", &(data->down_arrival_1), &(data->up_arrival_1)) != 2  || data->down_arrival_1 >= data->up_arrival_1 || data->down_arrival_1 < 0)
    {
        errno = WRONG_LIMITS;
        return ERROR;
    } 


    fprintf(out_stream, "Введите нижнюю и верхнюю границу времени обработки заявок 2 типа(в е.в, натуральные числа либо 0):\n");
    
    if (fscanf(in_stream, "%d %d", &(data->down_cycling_2), &(data->up_cycling_2)) != 2 || data->down_cycling_2 >= data->up_cycling_2 || data->down_cycling_2 < 0)
    {
        errno = WRONG_LIMITS;
        return ERROR;
    }

    return OK;
}

void set_default(range_t *const data)
{
    data->down_arrival_1 = DOWN_LIMIT_ARRIVAL_1;
    data->up_arrival_1 = UP_LIMIT_ARRIVAL_1;
    data->down_cycling_1 = DOWN_LIMIT_CYCLING_1;
    data->up_cycling_1 = UP_LIMIT_CYCLING_1;
    data->down_cycling_2 = DOWN_LIMIT_CYCLING_2;
    data->up_cycling_2 = UP_LIMIT_CYCLING_2;
}

void plus_timing_list(queue_t *const queue)
{
    list_t *head_copy = queue->head;

    while (head_copy)
    {
        (head_copy->waiting_time)++;
        head_copy = head_copy->next;
    }
}

int do_modeling_list(queue_t *const queue, range_t *const range, info_t *const info, int *const max_size_state, clock_t *const ticks_time)
{
    int recycled_1_type = 0, info_flag = 0, total_waiting_sum = 0;
    int time_arrival = 0, time_cycling = 0;
    double time_since_start = 0.000000, total_downtime = 0.000000;
    clock_t start_operation = 0, end_operation = 0;

    // Статистика
    int middle_length_sum = 0, delete_counter = 0, in_number = 0, in_1_type = 0, out_1_type = 0, in_2_type = 0;

    time_arrival = (int) round(1000000 * set_time((double) range->down_arrival_1 / 1000000, (double) range->up_arrival_1 / 1000000));
    //printf("%d - time_arrival\n", time_arrival);
    //time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_2 / 1000000, (double) range->up_cycling_2 / 1000000));
    //printf("%d - time_cycling\n", time_cycling);
    //printf("here1\n");
    start_operation = clock();
    add_to_tail_list(queue, TYPE_2, 0);
    end_operation = clock();
    (*ticks_time) += end_operation - start_operation;
    //printf("here2\n");
    //list_t *copy = queue->head;

    /*
    printf("%p\n",copy);
    while (copy)
    {   
        printf("her\n");
        printf("%d ", copy->type);
        copy = copy->next;
    }
    printf("\n");
    printf("%ld - size\n", queue->size);
    //add_to_tail_array(queue, TYPE_2);
    //add_to_tail_array(timing, 0);
    */
    in_number++;
    in_2_type++;

    while (recycled_1_type < TOTAL_CYCLED)
    {
        if (queue->size > *max_size_state)
        {
            *max_size_state = queue->size;
        }
        /*
        //printf("here3\n");
        if (recycled_1_type <= 1000)
        {
            list_t *copy = queue->head;
            printf("%p\n",copy);
            printf("here %d - time_arr, %d - time_cycl\n", time_arrival, time_cycling);
            while (copy)
            {
                //printf("her\n");
                printf("%d ", copy->type);
                copy = copy->next;
            }
            printf("\n");
            printf("%d - in_1_type\n", in_1_type);
            printf("%d - out_1_type\n", out_1_type);
            printf("%d - total_wait\n\n", total_waiting_sum);
            printf("%ld - size\n", queue->size);
        }
        */

        if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
        {
            info_flag = recycled_1_type;
            info->current_length = queue->size;
            info->length_mid = middle_length_sum / (float) delete_counter; 
            info->total_in_requests = in_number;
            info->total_out_requests = delete_counter;
            info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
            fprintf(stdout, "Всего обработано заявок - %d\n", recycled_1_type);   
            show_current_results(stdout, info);
            //printf("%d - in_1_type\n", in_1_type);
            //printf("%d - out_1_type\n\n", out_1_type);
        }

        time_since_start += 0.000001;
        //printf("%d\n", recycled_1_type);
        while (time_arrival == 0)
        {
            time_arrival = (int) round(1000000 * set_time((double) range->down_arrival_1 / 1000000, (double) range->up_arrival_1 / 1000000));
            //printf("%d - time_arrival\n", time_arrival);
            start_operation = clock();

            if (add_to_tail_list(queue, TYPE_1, 0) != OK)
            {
                return ERROR;
            }

            end_operation = clock();
            (*ticks_time) += end_operation - start_operation;
            /*
            if (add_to_tail_array(queue, TYPE_1) != OK)
            {
                return ERROR;
            }

            if (add_to_tail_array(timing, 0) != OK)
            {
                return ERROR;
            }*/

            in_number++;
            in_1_type++;
        }

        //printf("%f - time_arrival\n", time_arrival);
        time_arrival -= 1;

        while (time_cycling == 0 && queue->size != 0)
        {
            if (recycled_1_type >= TOTAL_CYCLED)
            {
                break;
            }

            middle_length_sum += queue->size;
            //middle_length_sum += queue->head - queue->tail;
            delete_counter++;
            //printf("%d - time_cycling\n", time_cycling);

            if (queue->head->type == TYPE_2)
            {
                time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_2 / 1000000, (double) range->up_cycling_2 / 1000000));
                in_2_type++;
                in_number++;
            }
            else
            {
                time_cycling = (int) round(1000000 * set_time((double) range->down_cycling_1 / 1000000, (double) range->up_cycling_1 / 1000000));
                out_1_type++;
                recycled_1_type++;
            }

            // Код связанный с подсчетом среднего времени --------------------

            total_waiting_sum += queue->head->waiting_time;
            // ---------------------------------------------------------------
            //printf("here5\n");
            start_operation = clock();

            if (remove_from_head_list(queue) != OK)
            {
                return ERROR;
            }

            end_operation = clock();
            (*ticks_time) += end_operation - start_operation;
            //printf("here6\n");

            if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
            {
                info_flag = recycled_1_type;
                info->current_length = queue->size;
                info->length_mid = middle_length_sum / (float) delete_counter; 
                info->total_in_requests = in_number;
                info->total_out_requests = delete_counter;
                info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
                fprintf(stdout, "Всего обработано заявок - %d\n", recycled_1_type);   
                show_current_results(stdout, info);
            }       
        }

        time_cycling -= 1;

        if (time_cycling < 0)
        {
            total_downtime += 0.000001;
        }

        plus_timing_list(queue);
    }

    if (recycled_1_type % 100 == 0 && recycled_1_type != 0 && recycled_1_type != info_flag)
    {
        info_flag = recycled_1_type;
        info->current_length = queue->size;
        info->length_mid = middle_length_sum / (float) delete_counter; 
        info->total_in_requests = in_number;
        info->total_out_requests = delete_counter;
        info->mid_time_in_queue = total_waiting_sum / (float) delete_counter;
        fprintf(stdout, "Всего обработано заявок - %d\n", recycled_1_type);   
        show_current_results(stdout, info);
        //printf("%d - in_1_type\n", in_1_type);
        //printf("%d - out_1_type\n\n", out_1_type);
    }

    info->modelling_time = time_since_start;
    info->machine_downtime = total_downtime;
    info->total_in_1_type = in_1_type;
    info->total_out_1_type = out_1_type;
    info->total_in_type_2 = in_2_type; 
    //printf("%f - middle\n", sum / (float) times);    
    return OK;
}