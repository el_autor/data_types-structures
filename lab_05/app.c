#include "app.h"

void show_menu()
{
    fprintf(stdout, "Выберите код операции:\n\
1) Провести моделирование(статичекий массив) и вывести результаты\n\
2) Провести моделирование(односвязный список) и вывести результаты\n\
3) Ввести время прибытия заявок и время обработки заявок\n\
4) Выйти их программы\n");
}

int show_error(const int err_code)
{
    switch (err_code)
    {
        case WRONG_KEY:
            printf("Неверный код операции!\n");
            break;
        case QUEUE_REFILLED:
            printf("Очередь переполнена!\n");
            break;
        case QUEUE_IS_EMPTY:
            printf("Очередь пуста!\n");
            break;
        case WRONG_LIMITS:
            printf("Неправильный ввод временных параметров!\n");
            break;
    }

    return err_code;
}

void greeting()
{
    printf("Программа для моделирования очереди с использованием:\n\
а) Статического массива\n\
б) Односвязного списка\n\n");
}

void refresh_data(array_queue_t *const attributes, int *const data)
{
    attributes->down_limit = data;
    attributes->up_limit = data + QUEUE_SIZE - 1;
    attributes->head = data + QUEUE_SIZE - 1;
    attributes->tail = data + QUEUE_SIZE - 1;
}

int main()
{
    bool process = true;
    int action = 0, max_size_state = 0;
    srand(time(NULL));
    info_t results;
    range_t range;
    make_zero(&results);
    clock_t operations_timing = 0;

    // Статический массив
    int queue[QUEUE_SIZE];
    int timing[QUEUE_SIZE];
    array_queue_t attributes = { queue, queue + QUEUE_SIZE - 1, queue + QUEUE_SIZE - 1, queue + QUEUE_SIZE - 1};
    array_queue_t timing_attributes = { timing, timing + QUEUE_SIZE - 1, timing + QUEUE_SIZE - 1, timing + QUEUE_SIZE - 1};
    set_default(&range);

    // Односвязный список
    queue_t queue_list = { NULL, 0 };

    greeting();
        
    while (process)
    {
        show_menu();

        if (fscanf(stdin, "%d", &action) != 1 || action < 1 || action > 4)
        {
            return show_error(WRONG_KEY);
        }

        switch (action)
        {
            case MODELING_ARRAY:
                make_zero(&results);

                if (do_modeling_array(&attributes, &timing_attributes, &range, &results, &max_size_state, &operations_timing) != OK)
                {
                    return show_error(errno);
                }

                show_common_results(stdout, &results, ARRAY);
                refresh_data(&attributes, queue);
                refresh_data(&timing_attributes, timing);
                printf("Затраты по памяти(байт) - %ld\n", max_size_state * sizeof(int));
                printf("Общее время моделирования со статическим массивом - %f\n", (float) operations_timing / CLOCKS_PER_SEC);
                max_size_state = 0;
                operations_timing = 0;
                break;
            case MODELING_LIST:
                make_zero(&results);

                if (do_modeling_list(&queue_list, &range, &results, &max_size_state, &operations_timing) != OK)
                {
                    return show_error(errno);
                }
                
                show_common_results(stdout, &results, LIST);
                free_list(&queue_list);
                queue_list.size = 0;
                queue_list.head = NULL;
                printf("Затраты по памяти(байт) - %ld\n", max_size_state * (sizeof(struct list_t) - sizeof(int)));
                printf("Общее время моделирования со списком - %f\n", (float) operations_timing / CLOCKS_PER_SEC);
                max_size_state = 0;
                operations_timing = 0; 
                break;
            case SETUP_RANGE:
                if (input_ranges(stdin, stdout, &range) != OK)
                {
                    return show_error(errno);
                }

                break;
            case EXIT:
                process = false;
                break;
        }
    }

    return OK;
}