#include "array_queue.h"

int add_to_tail_array(array_queue_t *const queue, const int type)
{
    if (queue->tail == queue->down_limit)
    {
        errno = QUEUE_REFILLED;
        return ERROR;
    }
    
    *(queue->tail) = type;
    (queue->tail)--;
    return OK;
}

int remove_from_head_array(array_queue_t *const queue)
{
    int type = 0;

    if (queue->tail == queue->head)
    {
        errno = QUEUE_IS_EMPTY;
        return ERROR;
    }

    type = *(queue->head); 

    for (int *p = queue->head; p > queue->tail + 1; p--)
    {
        *(p) = *(p - 1);
    }

    (queue->tail)++;

    if (type == TYPE_2)
    {
        if (queue->head - (queue->tail + 1) < 3)
        {
            //printf("here1\n");
            add_to_tail_array(queue, TYPE_2);
        }
        else
        {
            //printf("here2\n");
            for (int *p = (queue->tail + 1) - 1; p < (queue->head) - 3; p++)
            {
                *(p) = *(p + 1);
            }

            *((queue->head) - 3) = TYPE_2;
            (queue->tail)--;
        }
    }

    return OK;
}