#include "results.h"

void make_zero(info_t *const info)
{
    info->current_length = 0;
    info->length_mid = 0;
    info->machine_downtime = 0;
    info->mid_time_in_queue = 0;
    info->modelling_time = 0;
    info->total_in_1_type = 0;
    info->total_in_requests = 0;
    info->total_in_type_2 = 0;
    info->total_out_1_type = 0;
    info->total_out_requests = 0;
}

void show_common_results(FILE *const stream, const info_t *const info, const int modelling_mode)
{
    if (modelling_mode == ARRAY)
    {
        fprintf(stream, "Итоговая статистика реализации статическим массивом:\n");
    }
    else
    {
        fprintf(stream, "Итоговая статистика реализации списком:\n");        
    }
    
    fprintf(stream, "%d - общее время моделирования(е.в)\n", (int) (info->modelling_time * 1000000));
    fprintf(stream, "%d - время простоя аппарата(е.в)\n", (int) (info->machine_downtime * 1000000));
    fprintf(stream, "%d - кол-во вошедших 1 типа\n", info->total_in_1_type);
    fprintf(stream, "%d - кол-во вышедших 1 типа\n", info->total_out_1_type);
    fprintf(stream, "%d - кол-во обращений заявок 2 типа\n", info->total_in_type_2);
}

void show_current_results(FILE *const stream, const info_t *const info)
{
    fprintf(stream, "%d - текущая длина очереди\n", info->current_length);
    fprintf(stream, "%f - средняя длина очереди\n", info->length_mid);
    fprintf(stream, "%d - кол-во вошедших заявок\n", info->total_in_requests);
    fprintf(stream, "%d - кол-во вышедших заявок\n", info->total_out_requests);
    fprintf(stream, "%f - среднее время пребывания заявки в очереди(е.в)\n\n", info->mid_time_in_queue);
}