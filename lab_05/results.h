#ifndef _RESULTS_H_

#define _RESULTS_H_

#include <stdio.h>
#include "app.h"
#include "structs.h"

void make_zero(info_t *const info);
void show_common_results(FILE *const stream, const info_t *const info, const int modelling_mode);
void show_current_results(FILE *const stream, const info_t *const info);

#endif