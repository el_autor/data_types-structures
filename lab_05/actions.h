#ifndef _ACTIONS_H_

#define _ACTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include "structs.h"
#include "errors.h"
#include "results.h"
#include "array_queue.h"
#include "list_queue.h"

#define TOTAL_CYCLED 1000

#define DOWN_LIMIT_ARRIVAL_1 0 
#define UP_LIMIT_ARRIVAL_1 5
#define DOWN_LIMIT_CYCLING_1 0 
#define UP_LIMIT_CYCLING_1 4
#define DOWN_LIMIT_CYCLING_2 0 
#define UP_LIMIT_CYCLING_2 4


void set_default(range_t *const data);
int input_ranges(FILE *const in_stream, FILE *const out_stream, range_t *const data);
int do_modeling_array(array_queue_t *const queue, array_queue_t *const timing, range_t *const range, info_t *const info, int *const max_size_state, clock_t *const ticks_time);
int do_modeling_list(queue_t *const queue, range_t *const range, info_t *const info, int *const max_size_state, clock_t *const ticks_time);

#endif 