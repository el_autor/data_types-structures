#include "list_queue.h"

int add_to_tail_list(queue_t *queue, const int type, const int timing)
{
    list_t *node = NULL, *copy = queue->head;

    if ((node = malloc(sizeof(list_t))) == NULL)
    {
        errno = MEM_NOT_ALLOCED;
        return ERROR;
    }

    node->type = type;
    node->waiting_time = timing;
    node->next = NULL;

    if (copy)
    {
        while (copy->next)
        {
            copy = copy->next;
        }

        copy->next = node; 
    }
    else
    {
        queue->head = node;
    }
    
    (queue->size)++;
    return OK;
}

int remove_from_head_list(queue_t *const queue)
{
    list_t *copy = queue->head, *node = NULL;
    int type = 0;

    type = copy->type;
    queue->head = copy->next;
    free(copy);
    (queue->size)--;

    if (type == TYPE_2)
    {
        if (queue->size < 4)
        {
            add_to_tail_list(queue, TYPE_2, 0);
        }
        else
        {
            copy = queue->head;

            for (int i = 0; i < 2; i++)
            {
                copy = copy->next;
            }

            if ((node = malloc(sizeof(list_t))) == NULL)
            {
                errno = MEM_NOT_ALLOCED;
                return ERROR;
            }

            node->waiting_time = 0;
            node->type = TYPE_2;
            node->next = copy->next;
            copy->next = node;
            (queue->size)++;
        }
    }

    return OK;
}

void free_list(queue_t *const queue)
{
    list_t *copy_head = queue->head, *local = NULL;

    while (copy_head)
    {
        local = copy_head;
        copy_head = copy_head->next;
        free(local);
    }
}