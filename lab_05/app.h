#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include "errors.h"
#include "actions.h"
#include "structs.h"

#define ARRAY 1
#define LIST 2

#define MODELING_ARRAY 1
#define MODELING_LIST 2
#define SETUP_RANGE 3
#define EXIT 4

#endif