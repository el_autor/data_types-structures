#ifndef _LIST_QUEUE_H_

#define _LIST_QUEUE_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "structs.h"
#include "errors.h"

#define TYPE_1 1
#define TYPE_2 2

int add_to_tail_list(queue_t *const queue, const int type, const int timing);
int remove_from_head_list(queue_t *const queue);
void free_list(queue_t *const queue);

#endif