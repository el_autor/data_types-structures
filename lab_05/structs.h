#ifndef _STRUCTS_H_

#define _STRUCTS_H_

#define QUEUE_SIZE 5000

typedef struct
{
    int *down_limit;
    int *up_limit;
    int *head;
    int *tail;
} array_queue_t;

typedef struct
{  
    int current_length; // Текущая длина очереди
    float length_mid; // Средняя длина очереди
    int total_in_requests; // Кол-во вошедших заявок
    int total_out_requests; // Кол-во вышедших заявок
    float mid_time_in_queue; // Среднее время пребывания заявки в очереди
    float modelling_time; // Общее время моделирования
    float machine_downtime; // Время простоя аппарата
    int total_in_1_type; // Общее количество вошедших заявок 1 типа
    int total_out_1_type; // Общее количество вышедших заявок 1 типа
    int total_in_type_2; // Кол-во обращений заявки 2 типа
} info_t;

typedef struct 
{
    int down_cycling_1;
    int up_cycling_1;
    int down_arrival_1;
    int up_arrival_1;
    int down_cycling_2;
    int up_cycling_2;
} range_t;

struct list_t
{
    int type;
    int waiting_time;
    struct list_t *next;
};

typedef struct list_t list_t;

typedef struct
{
    list_t *head;
    size_t size;
} queue_t;



#endif