#ifndef _ARRAY_QUEUE_H_

#define _ARRAY_QUEUE_H_

#define TYPE_1 1
#define TYPE_2 2

#include <stdio.h>
#include <errno.h>
#include "errors.h"
#include "structs.h"

int add_to_tail_array(array_queue_t *const queue, const int type);
int remove_from_head_array(array_queue_t *const queue);

#endif