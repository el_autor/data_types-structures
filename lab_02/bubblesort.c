#include "struct.h"
#include "quicksort.h"

void bubble_sort(theatre *const data, const int size)
{
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (data[j].cost_range[0] > data[j+1].cost_range[0])
            {
                change(data, j, j + 1);
            }
        }
    }
}

void bubble_sort_keys(keys *const keys_data, const int size)
{
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (keys_data[j].key > keys_data[j+1].key)
            {
                change_int(keys_data, j, j + 1);
            }
        }
    }
}