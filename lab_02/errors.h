#ifndef _ERRORS_H_

#define _ERRORS_H_

#define OK 0
#define STRING_INPUT_OVERFLOW 1
#define COMMAND_INPUT_CODE 2
#define FILE_OPEN_ERROR 3
#define MEMORY_OVERFLOW 4
#define WRONG_RECORD_INPUT 5
#define WRONG_FORMAT 6
#define NO_DATA 7
#define NO_SUCH_DATA 8

#endif