#ifndef _INPUT_H_

#define _INPUT_H_

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "struct.h"
#include "errors.h"
#include "quicksort.h"
#include "bubblesort.h"

int choose_file(FILE *const stream, char *const filename);
int show_data(char *filename, int current_size);
int add_record(char *const filename, keys *const keys_data, theatre *const data, int *const counter);
int fill_from_file(char *const filename, keys *const keys_data, theatre *const data, int *const counter);
int delete_record(char *const filename, keys *const keys_data, theatre *const data_array, int *const current_size);
int show_keys(keys *const keys_data, const int size);
int show_array_no_keys(theatre *const data_array, const int size);
int show_array_keys(theatre *const data_array, keys *const keys_data, const int size);
int quicksort_no_keys(theatre *const data_array, const int size, long double *const time);
int bubblesort_no_keys(theatre *const data_array, const int size, long double *const time);
int quicksort_keys(keys *const keys_data, const int size, long double *const time);
int bubblesort_keys(keys *const keys_data, const int size, long double *const time);
int show_task(theatre *const data, const int size);

#endif