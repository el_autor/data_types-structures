#ifndef _STRUCT_H_

#define _STRUCT_H_

#define NAME_SIZE 21
#define RANGE 2

#define FAIRYTALE 1
#define CHILD_PLAY 2
#define ADULT_PLAY 1 
#define MUSICAL 3
#define DRAMA 2
#define COMEDY 3

#define CHILD_TYPE 1
#define ADULT_TYPE 2
#define MUSICAL_TYPE 3

#define DATA_SIZE 1000



typedef struct
{
    char theatre_name[NAME_SIZE];
    char name[NAME_SIZE];
    char producer[NAME_SIZE];
    int cost_range[RANGE];
    int type; // Show type

    union type
    {
        struct child
        {
            int age;
            union child_type
            {
                int type;
                struct musical
                {
                    char composer[NAME_SIZE];
                    char country[NAME_SIZE];
                    int min_age;
                    int duration;

                } musical;
                
            } child_type;

        } child;

        struct adult 
        {
            int type;

        } adult;

    } event_type;

} theatre;

typedef struct
{
    int order;
    int key;
} keys;

#endif
