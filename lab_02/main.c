#include <stdio.h>

#include "struct.h"
#include "errors.h"
#include "input.h"
#include "time.h"

#define DONE 1
#define UNDONE 0

#define EXIT_CODE 0
#define SHOW 1
#define SHOW_ARRAY_NO_KEYS 2
#define SHOW_ARRAY_KEYS 3 
#define SHOW_KEYS 4
#define ADD 5
#define DELETE 6
#define QUICKSORT_ARRAY_NO_KEYS 7
#define BUBBLESORT_ARRAY_NO_KEYS 8
#define QUICKSORT_ARRAY_KEYS 9
#define BUBBLESORT_ARRAY_KEYS 10
#define GET_SORT_ANALYSIS 11
#define GET_METHOD_ANALYSIS 12
#define TASK 13
#define FILL_START_DATA 14

// Глобальные переменные
char code_7 = 0, code_8 = 0, code_9 = 0, code_10 = 0;
long double time_qsort_no_keys = 0, time_bsort_no_keys = 0, time_qsort_keys = 0, time_bsort_keys = 0;
long double output_keys = 0, output_no_keys = 0;
long double filling_keys_time = 0;

void user_greeting()
{
    printf("                      Театральная афиша\n \
    Программа выводит список всех музыкальных спектаклей\n \
    для детей указанного возраста с продолжительностью меньше\n \
    указанной\n\n");
}

void show_info()
{
    printf("\
    Введите:\n\
    0 - выйти из программы\n\
    1 - показать исходные данные из файла\n\
    2 - вывод афиши без использования таблицы ключей\n\
    3 - вывод афиши с использованием таблицы ключей\n\
    4 - показать таблицу ключей\n\
    5 - добавить запись в конец\n\
    6 - удалить запись(по названию спектакля)\n\
    (Упорядочивание по возрастанию)\n\
    7 - упорядочить без ключей(quicksort)\n\
    8 - упорядочить без ключей(bubblesort)\n\
    9 - упорядочить используя таблицу ключей(quicksort)\n\
    10 - упорядочить используя таблицу ключей(bubblesort)\n\
    11 - сравнить эффективность сортировок(после пунктов 7, 8, 9, 10)\n\
    12 - сравнение эффективности обработки с таблицей ключей и без нее(после пунктов 7, 8, 9, 10)\n\
    13 - вывод детских музыкальных спектаклей\n\
    14 - заполнить исходными данными\n");
}

int error_message(const int code)
{
    switch (code)
    {
        case (STRING_INPUT_OVERFLOW):
        {
            printf("Неправильный ввод строки\n");
            return code;
        }
        case (COMMAND_INPUT_CODE):
        {
            printf("Неверная команда\n");
            return code;
        }
        case (FILE_OPEN_ERROR):
        {
            printf("Ошибка открытия файла\n");
            return code;
        }
        case (MEMORY_OVERFLOW):
        {
            printf("Недостаточно памяти\n");
            return code;
        }
        case (WRONG_RECORD_INPUT):
        {
            printf("Неверный формат ввода\n");
            return code;
        }
        case (WRONG_FORMAT):
        {
            printf("Неверный формат ввода\n");
            return code;
        }
        case (NO_DATA):
        {
            printf("В афише нет записей\n");
            return code;
        }
        case (NO_SUCH_DATA):
        {
            printf("Записи для удаления не найдены\n");
            return code;
        }
    }

    return OK;
}

void undone_all(char *const code_1, char *const code_2, char *const code_3, char *const code_4)
{
    *code_1 = UNDONE;
    *code_2 = UNDONE;
    *code_3 = UNDONE;
    *code_4 = UNDONE;
}

int all_done(const int code_1, const int code_2, const int code_3, const int code_4)
{
    if (code_1 == code_2 && code_3 == code_4 && code_1 == code_3 && code_1 == 1)
    {
        return DONE;
    }

    return UNDONE;
}

int do_action(theatre *const data_array, keys *const data_keys, char *const filename, int *const data_counter, const int command_code)
{
    int function_result = 0;

    switch (command_code)
    {
        case (SHOW):
        {
            if ((function_result = show_data(filename, *data_counter)) != OK)
            {
                return function_result;
            }

            break;
        } 
        case (ADD):
        {
            undone_all(&code_8, &code_9, &code_9, &code_10);
            if ((function_result = add_record(filename, data_keys, data_array, data_counter)) != OK)
            {
                return function_result;
            }

            break;
        }
        case (DELETE):
        {
            undone_all(&code_8, &code_9, &code_9, &code_10);
            if ((function_result = delete_record(filename, data_keys, data_array, data_counter)) != OK)
            {
                return function_result;
            }

            break;
        }
        case (SHOW_KEYS):
        {
            if (show_keys(data_keys, *data_counter) != OK)
            {
                printf("Таблица ключей пуста\n");
            }

            break;
        }    
        case (SHOW_ARRAY_NO_KEYS):
        {
            if (show_array_no_keys(data_array, *data_counter) != OK)
            {
                printf("Афиша пуста\n");
            }

            break;
        }
        case (SHOW_ARRAY_KEYS):
        {
            if (show_array_keys(data_array, data_keys, *data_counter) != OK)
            {
                printf("Афиша пуста\n");
            }

            break;
        }
        case (QUICKSORT_ARRAY_NO_KEYS):
        {
            if (quicksort_no_keys(data_array, *data_counter, &time_qsort_no_keys) != OK)
            {
                printf("Нечего сортировать\n");
            }
            else
            {
                code_7 = DONE;
            }
            
            break;
        }
        case (BUBBLESORT_ARRAY_NO_KEYS):
        {
            if (bubblesort_no_keys(data_array, *data_counter, &time_bsort_no_keys) != OK)
            {
                printf("Нечего сортировать\n");
            }
            else
            {
                code_8 = DONE;
            }

            break;
        }
        case (QUICKSORT_ARRAY_KEYS):
        {
            if (quicksort_keys(data_keys, *data_counter, &time_qsort_keys) != OK)
            {
                printf("Нечего сортировать\n");
            }
            else
            {
                code_9 = DONE;
            }
            
            break;
        }
        case (BUBBLESORT_ARRAY_KEYS):
        {
            if (bubblesort_keys(data_keys, *data_counter, &time_bsort_keys) != OK)
            {
                printf("Нечего сортировать\n");
            }
            else
            {
                code_10 = DONE;
            }

            break;
        }
        case (GET_SORT_ANALYSIS):
        {
            if (all_done(code_7, code_8, code_9, code_10))
            {
                printf("Размер таблицы - %d\nБыстрота сортировок:\n", *data_counter);

                printf("Bubblesort(исходная таблица) - %.10Lf\n", time_bsort_no_keys);

                printf("Bubblesort(таблица ключей) -   %.10Lf\n", time_bsort_keys);

                printf("Quicksort(исходная таблица) -  %.10Lf\n", time_qsort_no_keys);

                printf("Quicksort(таблица ключей) -    %.10Lf\n", time_qsort_keys);
            }
            else
            {
                printf("Выполните дейстия 7, 8, 9, 10 с одним набором данных!\n");
            }

            break;
        }
        case (GET_METHOD_ANALYSIS):
        {
            time_t start, end;
            if (all_done(code_7, code_8, code_9, code_10))
            {
                start = clock();
                show_array_keys(data_array, data_keys, *data_counter);
                end = clock();

                output_keys = (long double) (end - start) / CLOCKS_PER_SEC;

                start = clock();
                show_array_no_keys(data_array, *data_counter);
                end = clock();

                output_no_keys = (long double) (end - start) / CLOCKS_PER_SEC;                

                start = clock();
                for (int i = 0; i < *data_counter; i++)
                {
                    data_keys[i].order = i;
                    data_keys[i].key = data_array[i].cost_range[0];
                }
                end = clock();

                filling_keys_time = (long double) (end - start) / CLOCKS_PER_SEC;

                for (int i = 0; i < 50; i++)
                {
                    printf("\n");
                }

                printf("Итоги:\n");

                printf("Размер таблицы - %d\nБыстрота методов:\n", *data_counter);

                printf("Bubblesort(исходная таблица) + вывод на экран %.10Lf\n", time_bsort_no_keys + output_no_keys);

                printf("Bubblesort(таблица ключей) + заполнение таблицы ключей + вывод на экран - %.10Lf\n", time_bsort_keys + filling_keys_time + output_keys);

                printf("Quicksort(исходная таблица) + вывод на экран - %.10Lf\n", time_qsort_no_keys + output_no_keys);

                printf("Quicksort(таблица ключей) + заполнение таблицы ключей + вывод на экран - %.10Lf\n", time_qsort_keys + filling_keys_time + output_keys);

                printf("Память:\n");
                printf("Без ключей - %ld байт\n", sizeof(theatre) * *data_counter);
                printf("С ключами - %ld байт\n", sizeof(theatre) * *data_counter + sizeof(keys) * *data_counter);
            }
            else
            {
                printf("Выполните дейстия 7, 8, 9, 10 с одним набором данных!\n");
            }
            break;

        }
        case (FILL_START_DATA):
        {
            *data_counter = 0;
            if ((function_result = fill_from_file(filename, data_keys, data_array, data_counter)) != OK)
            {
                return error_message(function_result);
            }
            else
            {
                printf("Массив заполнен исходными данными!\n");    
            }
            
            break;
        }
        case (TASK):
        {
            if (show_task(data_array, *data_counter) != OK)
            {
                printf("Данные отсутствуют\n");
            }

            break;
        }
    }

    return OK;
}

int main()
{ 
    theatre data_array[DATA_SIZE];
    keys data_keys[DATA_SIZE]; // Упорядочивание по минимальному порогу
    char filename[NAME_SIZE]; // Имя файла для ввода данных
    int function_result = 0, loop = 1, command_code = 0, records_counter = 0;

    user_greeting();

    if ((function_result = choose_file(stdin, filename)) != OK) // Выбор файла для хранения данных
    {
        return error_message(function_result);
    }

    if ((function_result = fill_from_file(filename, data_keys, data_array, &records_counter)) != OK)
    {
        return error_message(function_result);
    }

    while (loop)
    {
        show_info();

        if (fscanf(stdin, "%d", &command_code) != 1 || command_code < 0 || command_code > 14)
        {
            return error_message(COMMAND_INPUT_CODE);
        }
        
        if (command_code == EXIT_CODE)
        {
            loop = 0;
        }

        if ((function_result = do_action(data_array, data_keys, filename, &records_counter, command_code)) != OK)
        {
            return error_message(function_result);
        }
    }

    printf("До встречи на спектаклe!\n");
    return OK;
}