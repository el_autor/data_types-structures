#include "struct.h"
#include "quicksort.h"

void change(theatre *const arr, const int first, const int second)
{
    theatre temp = arr[first];
    arr[first] = arr[second];
    arr[second] = temp;
}

void change_int(keys *const keys_data, const int first, const int second)
{
    keys temp = keys_data[first];
    keys_data[first] = keys_data[second];
    keys_data[second] = temp;
}

void quick_sort(theatre *const arr, const int head, const int tail)
{
    if (head >= tail)
    {
        return;
    }

    theatre mid = arr[(head + tail) / 2];
    int start = head, end = tail;

    while (start <= end)
    {
        while (arr[start].cost_range[0] < mid.cost_range[0])
        {
            start++;
        }
        while (arr[end].cost_range[0] > mid.cost_range[0])
        {
            end--;
        }
        if (start <= end)
        {
            change(arr, start, end);
            start++;
            end--;
        }
    }

    quick_sort(arr, head, end);
    quick_sort(arr, start, tail);
}

void quick_sort_keys(keys *const keys_data, const int head, const int tail)
{
    if (head >= tail)
    {
        return;
    }

    keys mid = keys_data[(head + tail) / 2];
    int start = head, end = tail;

    while (start <= end)
    {
        while (keys_data[start].key < mid.key)
        {
            start++;
        }
        while (keys_data[end].key > mid.key)
        {
            end--;
        }
        if (start <= end)
        {
            change_int(keys_data, start, end);
            start++;
            end--;
        }
    }

    quick_sort_keys(keys_data, head, end);
    quick_sort_keys(keys_data, start, tail);
}