#ifndef _QUICKSORT_H_

#define _QUICKSORT_H_

void quick_sort(theatre *arr, int head, int tail);
void quick_sort_keys(keys *const keys_data, const int head, const int tail);
void change(theatre *const arr, const int first, const int second);
void change_int(keys *const keys_data, const int first, const int second);

#endif 