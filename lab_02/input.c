#include "input.h"

#define KEY 0
#define NO_KEY 1

int musical_struct_input(struct musical *record);

void set_cursor(FILE *const file, const int index)
{
    theatre record;

    rewind(file);

    for (int i = 0; i < index; i++)
    {
        fread(&record, sizeof(theatre), 1, file);
    }
}

void set_by_pos(FILE *file, const theatre record, const int index)
{
    set_cursor(file, index);

    fwrite(&record, sizeof(theatre), 1, file);
}

theatre get_by_pos(FILE *file, const int index)
{
    theatre record;

    set_cursor(file, index);

    fread(&record, sizeof(theatre), 1, file);

    return record;
}

int limits_input(FILE *const stream, char *const filename)
{
    char counter = -1, symb = 0;

    while ((symb = getc(stream)) == ' ' || symb == '\n')

    if (symb == EOF)
    {
        return STRING_INPUT_OVERFLOW;
    }

    ungetc(symb, stream);

    while (counter++ < 20)
    {
        symb = getc(stream);

        if (symb == '\n' || symb == EOF)
        {
            break;
        }
        else
        {
            filename[(int) counter] = symb;
        }
    }

    if (counter == NAME_SIZE && symb != ' ' && symb != '\n' && symb != EOF)
    {
        return STRING_INPUT_OVERFLOW;
    }

    filename[(int) counter] = '\0';

    return OK;
}

int choose_file(FILE *const stream, char *const filename)
{
    printf("Введите имя файла-источника данных(не более 20 символов):\n");

    if (limits_input(stream, filename) == OK)
    {
        return OK;
    }
    
    return STRING_INPUT_OVERFLOW;
}

void show_record(theatre local_one)
{

    printf("|%-20s|%-20s|%-20s|%9d - %-9d|", local_one.theatre_name, local_one.name,
    local_one.producer, local_one.cost_range[0], local_one.cost_range[1]);

    switch (local_one.type)
    {
        case (CHILD_TYPE):
        {
            printf("    Child    |%7d        |", local_one.event_type.child.age);

            if (local_one.event_type.child.child_type.type == FAIRYTALE)
            {
                printf("  Fairytale  |");
            }
            else if (local_one.event_type.child.child_type.type == CHILD_PLAY)
            {
                printf("     Play    |");
            }
            else
            {
                printf("   Musical   |");
                printf("%-20s|", local_one.event_type.child.child_type.musical.composer);
                printf("%-20s|", local_one.event_type.child.child_type.musical.country);
                printf("         %-10d|", local_one.event_type.child.child_type.musical.min_age);
                printf("     %-7d|", local_one.event_type.child.child_type.musical.duration);
            }

            printf("\n");
            break;
        }
        case (ADULT_TYPE):
        {
            printf("    Adult    |       -       |");

            if (local_one.event_type.adult.type == ADULT_PLAY)
            {
                printf("     Play    |");
            }
            else if (local_one.event_type.adult.type == DRAMA)
            {
                printf("     Drama   |");
            }
            else
            {
                printf("    Comedy   |");
            } 

            printf("         -          |          -         |         -         |      -     |\n");
            break;           
        }
    }

}

void print_line(const int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("-");
    }

    printf("\n");
}

int show_data(char *filename, int data_counter)
{
    FILE *input_file = NULL;
    theatre local_one;

    //printf("data counter %d\n", data_counter);
    if (!data_counter)
    {
        printf("Афиша пуста\n");
        return OK;
    }

    if ((input_file = fopen(filename, "rb")))
    {
        print_line(205);
        printf("|        Театр       | Название спектакля |       Режиссер     |  Ценовой диапазон   |\
Тип спектакля|Возраст ребенка|     Тип     |     Композитор     |        Страна      |Минимальный возраст|Длительность|\n");

        while (data_counter > 0)
        {
            fread(&local_one, sizeof(theatre), 1, input_file);
            print_line(205);
            show_record(local_one);
            data_counter--;
        }

        print_line(205);
        fclose(input_file);
    } 
    else
    {
        return FILE_OPEN_ERROR;
    }

    return OK;    
}

int fill_from_file(char *const filename, keys *const data_keys, theatre *const data, int *const counter)
{
    FILE *input_file = NULL;
    theatre record;

    if ((input_file = fopen(filename, "rb")))
    {
        while (fread(&record, sizeof(theatre), 1, input_file) == 1 && record.cost_range[0] != - 1)
        {
            data[*counter] = record;

            data_keys[*counter].order = *counter;
            data_keys[*counter].key = record.cost_range[0];

            (*counter)++;

            if (*counter == DATA_SIZE)
            {
                return MEMORY_OVERFLOW;
            }
        }

        fclose(input_file);
    }
    else
    {
        return FILE_OPEN_ERROR;
    }
    
    return OK;
}

int range_input(int *cost_range)
{
    if (scanf("%d - %d", cost_range, cost_range + 1) != 2 || cost_range[0] < 0 || cost_range[1] < 0 || cost_range[0] > cost_range[1])
    {
        return WRONG_FORMAT;
    }

    return OK;
}

int type_input(int *const type)
{
    if (scanf("%d", type) != 1 || *type < 1 || *type > 2)
    {
        return WRONG_FORMAT;
    }

    return OK;
}

int child_struct_input(struct child *record)
{
    int type = 0, function_result = 0;;

    printf("\nВозраст для ребенка(неотрицательное число):");

    if (scanf("%d", &(record -> age)) != 1 || record -> age < 0 || record -> age > 17)
    {
        return WRONG_FORMAT;
    }

    printf("\nЖанр спектакля(введите цифру):\
    1 - сказка\
    2 - пьеса\
    3 - музыкальный\n");

    if (scanf("%d", &type) != 1 || type < 1 || type > 3)
    {
        return WRONG_FORMAT;
    }

    if (type < 3)
    {
        record -> child_type.type = type;
    }
    else
    {
        if ((function_result = musical_struct_input(&(record -> child_type.musical))) != OK)
        {
            return function_result;
        }
    }

    return OK;
}

int adult_struct_input(struct adult *record)
{
    printf("\nЖанр спектакля(введите цифру):\
    1 - пьеса\
    2 - драма\
    3 - комедия\n");

    if (scanf("%d", &(record -> type)) != 1 || record -> type < 1 || record -> type > 3)
    {
        return WRONG_FORMAT;
    }

    return OK;
}

int musical_struct_input(struct musical *record)
{
    int function_result = 0;

    printf("\nКомпозитор(не более 20 символов):");

    if ((function_result = limits_input(stdin, record -> composer)) != OK)
    {
        return function_result;
    }

    printf("\nСтрана(не более 20 символов):");

    if ((function_result = limits_input(stdin, record -> country)) != OK)
    {
        return function_result;
    }

    printf("\nМинимальный возараст(0 - 150):");

    if (fscanf(stdin, "%d", &(record -> min_age)) != 1 || record -> min_age < 0 || record -> min_age > 150)
    {
        return WRONG_FORMAT;
    }

    printf("\nПродолжительность спектакля(в минутах 10 - 300):");

    if (fscanf(stdin, "%d", &(record  -> duration)) != 1 || record->duration < 10 || record -> duration > 300)
    {
        return WRONG_FORMAT;
    }

    return OK;
}

int input_record(theatre *record)
{
    int function_result = 0;

    printf("Введите информацию о спектакле:\n");

    printf("Название театра(не более 20 символов):");

    if ((function_result = limits_input(stdin, record -> theatre_name)) != OK)
    {
        return function_result;
    }

    printf("Название спектакля(не более 20 символов):");

    if ((function_result = limits_input(stdin, record -> name)) != OK)
    {
        return function_result;
    }

    printf("\nРежиссер спектакля(не более 20 символов):");

    if ((function_result = limits_input(stdin, record -> producer)) != OK)
    {
        return function_result;
    }

    printf("\nДиапазон цен(через дефис):");

    if ((function_result = range_input(record -> cost_range)) != OK)
    {
        return function_result;
    }
    
    printf("\nТип спектакля(введите цифру):\
    1 - детский\
    2 - взрослых\n");
    
    if ((function_result = type_input(&(record -> type))) != OK)
    {
        return function_result;
    }

    switch (record -> type)
    {
        case (CHILD_TYPE):
        {
            if ((function_result = child_struct_input(&(record -> event_type.child))) != OK)
            {
                return function_result;
            }

            break;
        }
        case (ADULT_TYPE):
        {
            if ((function_result = adult_struct_input(&(record -> event_type.adult))) != OK)
            {
                return function_result;
            }

            break;
        }
    }

    return OK;
}

int add_record(char *const filename, keys *const keys_data, theatre *const data_array, int *const current_size)
{
    FILE *input_file = NULL;
    theatre record;

    if (*current_size == DATA_SIZE)
    {
        return MEMORY_OVERFLOW;
    }

    if ((input_file = fopen(filename, "rb+")))
    {
        if (input_record(&record) != OK)
        {
            return WRONG_RECORD_INPUT;
        }

        set_cursor(input_file, *current_size);
        fwrite(&record, sizeof(theatre), 1, input_file);
        data_array[*current_size] = record;
        keys_data[*current_size].order = *current_size;
        keys_data[*current_size].key = record.cost_range[0]; // минимальный порог цены
        (*current_size)++;
        fclose(input_file);
    }
    else
    {
        return FILE_OPEN_ERROR;
    }

    return OK;
}

void deletion_from_file(FILE *const file, const int index, const int current_size)
{
    set_cursor(file, index);
    
    for (int i = index; i < current_size - 1; i++)
    {
        set_by_pos(file, get_by_pos(file, i + 1), i);
    }
}

void deletion_from_array(theatre *const data_array, const int index, const int current_size)
{
    for (int i = index; i < current_size - 1; i++)
    {
        data_array[i] = data_array[i + 1];
    }
}

void deletion_from_keys(keys *const keys_data, const int index, int current_size)
{
    for (int i = 0; i < current_size; i++)
    {
        printf("%d\n", i);   
        if (keys_data[i].order == index)
        {
            for (int j = i; j < current_size - 1; j++)
            {
                keys_data[j] = keys_data[j + 1];
            }

            if (i != 0)
            {
                i--;
                current_size--;
            }
        }
    }

    for (int i = 0; i < current_size; i++)
    {
        if (keys_data[i].order > index)
        {
            keys_data[i].order--;
        }
    }  
}

int delete_record(char *const filename, keys *const keys_data, theatre *const data_array, int *const current_size)
{
    FILE *input_file = NULL;
    theatre record, stopper_record;
    char word[NAME_SIZE];
    int function_result = 0, flag = 0;

    if (*current_size == 0)
    {
        printf("Нечего удалять\n");
        return OK;
    }

    printf("Введите название спектакля(не более 20 символов):\n");

    if ((function_result = limits_input(stdin, word)) != OK)
    {
        return function_result;
    }

    if ((input_file = fopen(filename, "rb+")))
    {
        int index = 0;

        while (index != *current_size)
        {
            fread(&record, sizeof(record), 1, input_file);

            if (!strcmp(record.name, word))
            {
                flag = 1;

                deletion_from_file(input_file, index, *current_size);
                deletion_from_array(data_array, index, *current_size);
                deletion_from_keys(keys_data, index, *current_size);
                (*current_size)--;
                set_cursor(input_file, index--);
            }

            index++;
        }

        set_cursor(input_file, *current_size);
        stopper_record.cost_range[0] = -1;
        fwrite(&stopper_record, sizeof(theatre), 1, input_file);
        fclose(input_file);
    }

    if (!flag)
    {
        return NO_SUCH_DATA;
    }

    return OK;
}

int show_keys(keys *const keys_data, const int size)
{
    if (!size)
    {
        return NO_DATA;
    }

    print_line(38);
    printf("| Индекс в таблице | Значение ключа |\n");

    for (int i = 0; i < size; i++)
    {
        print_line(38);
        printf("|%17d | %-14d |\n", keys_data[i].order, keys_data[i].key);
    }
    
    print_line(38);
    return OK;
}

int show_array_no_keys(theatre *const data_array, const int size)
{
    if (!size)
    {
        return NO_DATA;
    }

    print_line(205);
    printf("|        Театр       | Название спектакля |       Режиссер     |  Ценовой диапазон   |\
Тип спектакля|Возраст ребенка|     Тип     |     Композитор     |        Страна      |Минимальный возраст|Длительность|\n");

    for (int i = 0; i < size; i++)
    {
        print_line(205);
        show_record(data_array[i]);
    }

    print_line(205);
    return OK;
}

int show_array_keys(theatre *const data_array, keys *const keys_data, const int size)
{
    if (!size)
    {
        return NO_DATA;
    }

    for (int i = 0; i < size; i++)
    {
        print_line(205);
        show_record(data_array[keys_data[i].order]);
    }

    print_line(205);
    return OK;
}

int quicksort_no_keys(theatre *const data_array, const int size, long double *const time)
{
    if (!size)
    {
        return NO_DATA;
    }
    
    clock_t start = clock();
    quick_sort(data_array, 0, size - 1);
    clock_t end = clock();

    *time = (long double) (end - start) / CLOCKS_PER_SEC;

    return OK;
}

int bubblesort_no_keys(theatre *const data_array, const int size, long double *const time)
{
    if (!size)
    {
        return NO_DATA;
    }

    clock_t start = clock();
    bubble_sort(data_array, size);
    clock_t end = clock();

    *time = (long double) (end - start) / CLOCKS_PER_SEC;

    return OK;
}

int quicksort_keys(keys *const keys_data, const int size, long double *const time)
{
    if (!size)
    {
        return NO_DATA;
    }

    clock_t start = clock();
    quick_sort_keys(keys_data, 0, size - 1);
    clock_t end = clock();

    *time = (long double) (end - start) / CLOCKS_PER_SEC;

    return OK;
}

int bubblesort_keys(keys *const keys_data, const int size, long double *const time)
{
    if (!size)
    {
        return NO_DATA;
    }

    clock_t start = clock();
    bubble_sort_keys(keys_data, size);
    clock_t end = clock();

    *time = (long double) (end - start) / CLOCKS_PER_SEC;

    return OK;
}

int show_task(theatre *const data, const int size)
{
    int age = 0, duration = 0, counter = 0;

    if (!size)
    {
        return NO_DATA;
    }

    printf("Возраст ребенка:\n");

    if (scanf("%d", &age) != 1 || age < 0 || age > 17)
    {
        return WRONG_FORMAT;
    }

    printf("Длительность:\n");

    if (scanf("%d", &duration) != 1 || duration < 0)
    {
        return WRONG_FORMAT;
    }

    print_line(205);
    printf("|        Театр       | Название спектакля |       Режиссер     |  Ценовой диапазон   |\
Тип спектакля|Возраст ребенка|     Тип     |     Композитор     |        Страна      |Минимальный возраст|Длительность|\n");
    print_line(205);

    for (int i = 0; i < size; i++)
    {
        if (data[i].type == CHILD_TYPE && data[i].event_type.child.child_type.musical.min_age <= age && data[i].event_type.child.child_type.musical.duration <= duration)
        {
            counter++;
            show_record(data[i]);
            print_line(205);
        }
    }
    
    if (counter > 0)
    {
        return OK;
    }
    else
    {
        return NO_DATA;
    }
    
}