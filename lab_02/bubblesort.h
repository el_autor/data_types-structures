#ifndef _BUBBLESORT_H_

#define _BUBBLESORT_H_

#include "struct.h"

void bubble_sort(theatre *const data, const int size);
void bubble_sort_keys(keys *const keys_data, const int size);

#endif