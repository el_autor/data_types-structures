#include "struct.h"
#include <stdio.h>


#define DATA_FILE "base_1.bin"
#define DATA_SIZE 1000
#define ITERATIONS 10

void set_cursor(FILE *const file, const int index)
{
    theatre record;

    rewind(file);

    for (int i = 0; i < index; i++)
    {
        fread(&record, sizeof(theatre), 1, file);
    }
}

void set_by_pos(FILE *file, const theatre record, const int index)
{
    set_cursor(file, index);

    fwrite(&record, sizeof(theatre), 1, file);
}

theatre get_by_pos(FILE *file, const int index)
{
    theatre record;

    set_cursor(file, index);

    fread(&record, sizeof(theatre), 1, file);

    return record;
}

int main()
{
    FILE *file  = fopen(DATA_FILE, "rb+");
    theatre data;

    for (int i = 1; i < ITERATIONS; i++)
    {
        for (int j = 0; j < 50; j++)
        {
            set_cursor(file, j);
            fread(&data, sizeof(theatre), 1, file);
            set_cursor(file, i * 50 + j);
            fwrite(&data, sizeof(theatre), 1, file);
        }
    }

    fclose(file);
}