#include <errno.h>
#include <time.h>
#include "structs.h"
#include "input.h"
#include "errors.h"
#include "show.h"
#include <stdio.h>

void transporate_sparse(sparse_matrix *const sparse_one)
{
    sparse_matrix local;

    local.rows = sparse_one -> cols;
    local.cols = sparse_one -> rows;

    int rows_counter = 0;

    for (int i = 0; i < sparse_one -> cols; i++)
    {
        local.rows_start[i] = rows_counter;

        for (int j = 0; j < sparse_one -> rows_start[sparse_one -> rows]; j++)
        {
            if (sparse_one -> cols_number[j] == i)
            {
                local.values[rows_counter] = sparse_one -> values[j];
                for (int i = 0; i < sparse_one -> rows; i++)
                {
                    if (j >= sparse_one -> rows_start[i] && j < sparse_one -> rows_start[i + 1])
                    {
                        local.cols_number[rows_counter] = i;
                        break;
                    }
                }
                rows_counter++;   
            }
        }
    }

    local.rows_start[local.rows] = rows_counter;

    *(sparse_one) = local;
}

void make_unreal_value(int *arr, const int cell)
{
    for (int i = 0; i < cell; i++)
    {
        arr[i] = -1;
    }
}

int multiply_sparse(sparse_matrix *const result, const sparse_matrix sparse_one, sparse_matrix sparse_vector, long double *const time)
{
    int buffer[MAX_SIZE];

    //printf("%d %d\n", sparse_one.cols, sparse_vector.rows);

    if (sparse_one.cols != sparse_vector.rows)
    {
        errno = WRONG_MULTIPLY_DIMS;
        return ERROR;
    }

    transporate_sparse(&sparse_vector);

    result -> rows = sparse_vector.rows;
    result -> cols = sparse_one.rows;

    int fill_index = 0;

    make_unreal_value(buffer, sparse_one.cols); 

    clock_t start = clock();

    for (int i = 0; i < sparse_one.rows; i++)
    {
        for (int k = sparse_one.rows_start[i]; k < sparse_one.rows_start[i + 1]; k++)
        {
            buffer[sparse_one.cols_number[k]] = k;
        }

        /*printf("show buffer\n");
        for (int i = 0; i < sparse_one.cols; i++)
        {
            printf("%d ", i);
        }

        printf("here\n");*/

        for (int j = 0; j < sparse_vector.rows; j++)
        {
            int sum = 0;
            result -> rows_start[i] = fill_index;

            for (int k = sparse_vector.rows_start[j]; k < sparse_vector.rows_start[j + 1]; k++)
            {
                //printf("here- 1\n");
                if (buffer[sparse_vector.cols_number[k]] != -1)
                {
                    //printf("%d - %d\n", sparse_one.values[buffer[sparse_one.cols_number[k]]], sparse_vector.values[k]);
                    sum += sparse_one.values[buffer[sparse_one.cols_number[k]]] * sparse_vector.values[k]; 
                }
            }

            if (sum)
            {
                //printf("%d - index\n", fill_index);
                result -> values[fill_index] = sum;
                result -> cols_number[fill_index] = i;
                fill_index++;
            } 
            //printf("here -4\n");
        }
 
        make_unreal_value(buffer, sparse_one.cols);
    }

    result -> rows_start[result -> rows] = fill_index;

    clock_t end = clock();
    transporate_sparse(result);

    *time = (long double) (end - start) / CLOCKS_PER_SEC;

    return OK;
}

int multiply_full(full_matrix *const result, const full_matrix full_one, const full_matrix full_vector, long double *const time)
{
    int sum = 0;

    if (full_one.cols != full_vector.rows)
    {
        //printf("%d - %d\n", full_one.cols, full_vector.rows);
        errno = WRONG_MULTIPLY_DIMS;
        return ERROR;
    }

    result -> rows = full_one.rows;
    result -> cols = full_vector.cols;

    clock_t start = clock();

    for (int i = 0; i < full_one.rows; i++)
    {
        for (int j = 0; j < full_vector.cols; j++)
        {
            sum = 0;

            for (int k = 0; k < full_one.cols; k++)
            {
                sum += full_one.matrix[i][k] * full_vector.matrix[k][j];
            }

            result -> matrix[i][j] = sum;
        }
    }

    clock_t  end = clock();
    *time = (long double) (end - start) / CLOCKS_PER_SEC;
    return OK;
}