#ifndef _INPUT_H_

#define _INPUT_H_

#include "structs.h"

int input_standart(full_matrix *const full_one, full_matrix *const vector, const int key);
int translate_to_sparse(const full_matrix full_one, sparse_matrix *const sparse_one);
int input_random(full_matrix *const full_one, full_matrix *const vector);

#define NAME_SIZE 20

#define STANDART 1
#define COORDINATE 2

#endif