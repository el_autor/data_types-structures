#include <stdio.h>
#include <stdbool.h>
#include "structs.h"
#include "show.h"

void print_line(const int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("-");
    }

    printf("\n");
}

void show_full_standart(const full_matrix full_one)
{
    print_line(5 * full_one.cols);

    for (int i = 0; i < full_one.rows; i++)
    {
        for (int j = 0; j < full_one.cols; j++)
        {
            printf("%-5d", full_one.matrix[i][j]);
        }

        printf("\n");
        print_line(5 * full_one.cols);
    }
}

void show_full_coordinate(const full_matrix full_one)
{
    bool flag = false;
    printf("Размер матрицы: [%d, %d]\n", full_one.rows, full_one.cols);
    printf("Вывод в формате [строка, столбец, значение]\n");

    for (int i = 0; i < full_one.rows; i++)
    {
        flag = false;

        for (int j = 0; j < full_one.cols; j++)
        {
            if (full_one.matrix[i][j] != 0)
            {
                flag = true;
                printf("| %d %d %d ", i, j, full_one.matrix[i][j]);
            }
        }

        if (flag)
        {
            printf("|\n");    
        }
    }
}

void show_sparse_coordinate(const sparse_matrix sparse_one)
{
    printf("Размер матрицы: [%d, %d]\n", sparse_one.rows, sparse_one.cols);
    printf("Вывод в формате [строка, столбец, значение]\n");

    for (int i = 1; i <= sparse_one.rows; i++)
    {
        for (int j = sparse_one.rows_start[i - 1]; j < sparse_one.rows_start[i]; j++)
        {
            printf("| %d %d %d ", i - 1, sparse_one.cols_number[j], sparse_one.values[j]);
        }

        if (sparse_one.rows_start[i] - sparse_one.rows_start[i - 1] > 0)
        {
            printf("|\n");
        }
    }
}

int search(const sparse_matrix sparse_one, const int i, const int j)
{
    for (int k = sparse_one.rows_start[i]; k < sparse_one.rows_start[i + 1]; k++)
    {
        if (sparse_one.cols_number[k] == j)
        {
            return sparse_one.values[k];
        }
    }

    return 0;
}

void show_sparse_standart(const sparse_matrix sparse_one)
{
    print_line(5 * sparse_one.cols);

    for (int i = 0; i < sparse_one.rows; i++)
    {
        for (int j = 0; j < sparse_one.cols; j++)
        {
            printf("%-5d", search(sparse_one, i , j));
        }

        printf("\n");
        print_line(5 * sparse_one.cols);
    }
}

int get_percent(const full_matrix *const full_one)
{
    int counter = 0;
    for (int i = 0; i < full_one -> rows; i++)
    {
        for (int j = 0; j < full_one -> cols; j++)
        {
            if (full_one -> matrix[i][j] != 0)
            {
                counter++;
            }
        }
    }

    return (int) (100 * ((float) counter / (full_one -> rows * full_one -> cols))); 
}

void show_stats(const full_matrix *const full_one,\
                const full_matrix *const vector,\
                const full_matrix *const result_one,\
                const sparse_matrix *const sparse_one,\
                const sparse_matrix *const sparse_vector,\
                const long double time_full,\
                const long double time_sparse)
{
    printf("Статистика методов\n");
    printf("Размерность %d*%d\n", full_one -> rows, full_one -> cols);
    printf("Степень заполненности матрицы - %d\n", get_percent(full_one));
    printf("Степень заполненности вектора-столбца - %d\n", get_percent(vector));
    printf("Степень заполненности результирующего вектора-столбцаа - %d\n", get_percent(result_one));
    printf("Время\n");
    printf("Время стандартного умножения - %Lf\n", time_full);
    printf("Время разряженного умножения - %Lf\n", time_sparse);


    size_t memory_full = sizeof(int) * full_one -> rows * full_one -> cols + sizeof(int) * full_one -> rows;
    size_t memory_sparse = 2 * sizeof(int) * sparse_one -> rows_start[sparse_one -> rows] + sizeof(int) * (sparse_one -> rows + 1) +\
    2 * sizeof(int) * sparse_vector -> rows_start[sparse_vector -> rows] + sizeof(int) * (sparse_vector -> rows + 1); 

    printf("Память\n");
    printf("Память для стандартного умножения - %ld\n", memory_full);
    printf("Память для разреженного умножения - %ld\n", memory_sparse);
}

void show_in_sparse(sparse_matrix sparse_one)
{
    printf("Размерность %d*%d\n", sparse_one.rows, sparse_one.cols);

    printf("Массив значений\n");

    for (int i = 0; i < sparse_one.rows_start[sparse_one.rows]; i++)
    {
        printf("%-5d", sparse_one.values[i]);
    }

    printf("\n");

    printf("Номера столбцов элементов\n");

    for (int i = 0; i < sparse_one.rows_start[sparse_one.rows]; i++)
    {
        printf("%-5d", sparse_one.cols_number[i]);
    }

    printf("\n");

    printf("Индексы начала каждой строки\n");

    for (int i = 0; i <= sparse_one.rows; i++)
    {
        printf("%-5d", sparse_one.rows_start[i]);
    }   

    printf("\n");
}