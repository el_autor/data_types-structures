#ifndef _STRUCTS_H_

#define _STRUCTS_H_

#define MAX_SIZE 200

typedef struct
{
    int values[MAX_SIZE * MAX_SIZE];
    int cols_number[MAX_SIZE * MAX_SIZE];
    int rows_start[MAX_SIZE + 1];
    int rows;
    int cols;

} sparse_matrix;

typedef struct
{
    int rows;
    int cols;
    int matrix[MAX_SIZE][MAX_SIZE];

} full_matrix;


#endif