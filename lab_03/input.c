#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "structs.h"
#include "input.h"
#include "errors.h"

int limits_input(FILE *const stream, char *const filename)
{
    char counter = -1, symb = 0;

    while ((symb = getc(stream)) == ' ' || symb == '\n')

    if (symb == EOF)
    {   
        return STRING_INPUT_OVERFLOW;
    }   

    ungetc(symb, stream);

    while (counter++ < 20) 
    {   
        symb = getc(stream);

        if (symb == '\n' || symb == EOF)
        {
            break;
        }
        else
        {
            filename[(int) counter] = symb;
        }
    }   

    if (counter == NAME_SIZE && symb != ' ' && symb != '\n' && symb != EOF)
    {   
        return STRING_INPUT_OVERFLOW;
    }   

    filename[(int) counter] = '\0';

    return OK; 
}

void transporate(full_matrix *const full_one)
{
    int local_array[full_one -> rows][full_one -> cols], local;

    for (int i = 0; i < full_one -> rows; i++)
    {
        for (int j = 0; j < full_one -> cols; j++)
        {
            local_array[j][i] = full_one -> matrix[i][j];
        }
    }

    local = full_one -> cols;
    full_one -> cols = full_one -> rows;
    full_one ->rows = local;

    for (int i = 0; i < full_one -> rows; i++)
    {
        for (int j = 0; j < full_one -> cols; j++)
        {
            full_one -> matrix[i][j] = local_array[i][j];
        }
    }
}

int get_structures(FILE *const stream, full_matrix *const full_one, full_matrix *const vector)
{
    printf("Введите количество строк матрицы(максимум - %d)\n", MAX_SIZE);

    if (fscanf(stream, "%d", &(full_one -> rows)) != 1 || full_one -> rows < 1 || full_one -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    printf("Введите количество столбцов матрицы(максимум - %d)\n", MAX_SIZE);

    if (fscanf(stream, "%d", &(full_one -> cols)) != 1 || full_one -> cols < 1 || full_one -> cols > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    printf("Введите элементы матрицы:\n");

    for (int i = 0; i < full_one -> rows; i++)
    {
        for (int j = 0; j < full_one -> cols; j++)
        {
            if (fscanf(stream, "%d", *(full_one -> matrix + i) + j) != 1)
            {
                errno = WRONG_ELEM_INPUT;
                return ERROR;
            }
        }
    }

    printf("Введите длину вектора столбца(максимум - %d)\n", MAX_SIZE);

    if (fscanf(stream, "%d", &(vector -> rows)) != 1 || vector -> rows < 1 || vector -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    vector -> cols = 1;

    printf("Введите элементы вектора - столбца:\n");

    for (int i = 0; i < vector -> rows; i++)
    {
        if (fscanf(stream, "%d", &(vector -> matrix[i][0])) != 1)
        {
            errno = WRONG_ELEM_INPUT;
            return ERROR;
        }
    }

    return OK;
}

int input_coord(FILE *const stream, full_matrix *const full_one)
{   
    int i = 0, j = 0, value = 0;
    printf("Вводите по 3 числа - строка, столбец, значения элемента(признак окончания - ввод -1)\n");

    while (fscanf(stream, "%d", &i) == 1)
    {
        if (i == -1)
        {
            return OK;
        }

        if (i >= full_one -> rows || i < 0)
        {
            //printf("here - 1\n");
            errno = INDEX_OVERFLOW;
            return ERROR;
        }

        if (fscanf(stream, "%d", &j) != 1)
        {
            //printf("here - 2\n");
            errno = WRONG_ELEM_INPUT;
            return ERROR;
        }

        if (j >= full_one -> cols || j < 0)
        {
            //printf("here - 3\n");
            errno = INDEX_OVERFLOW;
            return ERROR;
        }

        if (fscanf(stream, "%d", &value) != 1)
        {
            //printf("here - 4\n");
            errno = WRONG_ELEM_INPUT;
            return ERROR;
        }

        full_one -> matrix[i][j] = value;
    }

    //printf("here\n");
    errno = WRONG_ELEM_INPUT;
    return ERROR;
}

void fill_zero(full_matrix *const full_one)
{
    for (int i = 0; i < full_one -> rows; i++)
    {
        for (int j = 0; j < full_one -> cols; j++)
        {
            full_one -> matrix[i][j] = 0;
        }
    }
}

int get_coordinates(FILE *const stream, full_matrix *const full_one, full_matrix *const vector)
{
    int error_code = 0;

    printf("Введите размерность матрицы(максимум - %d)\n", MAX_SIZE);

    if (fscanf(stream, "%d", &(full_one -> rows)) != 1 || full_one -> rows < 1 || full_one -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    if (fscanf(stream, "%d", &(full_one -> cols)) != 1 || full_one -> cols < 1 || full_one -> cols > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    fill_zero(full_one);

    if ((error_code = input_coord(stream, full_one)) != OK)
    {
        return error_code;
    }

    printf("Введите длину вектора(максимум - %d)\n", MAX_SIZE);

    vector -> cols = 1;

    if (fscanf(stream, "%d", &(vector -> rows)) != 1 || vector -> rows < 1 || vector -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    fill_zero(vector);

    if ((error_code = input_coord(stream, vector)) != OK)
    {
        return error_code;
    }

    return OK;
}

int input_standart(full_matrix *const full_one, full_matrix *const vector, const int key)
{
    int input_code = 0, error_code = 0;
    FILE *stream = stdin;
    char filename[NAME_SIZE];

    printf("Введите способ ввода(цифра):\n\
1 - консоль\n\
2 - файл\n");

    if (scanf("%d", &input_code) != 1 || input_code < 1 || input_code > 2)
    {
        errno = WRONG_INPUT_CODE; 
        return ERROR;
    }

    if (input_code == 2)
    {
        printf("Введите имя файла\n");

        if ((error_code = limits_input(stdin, filename)) != OK)
        {
            errno = error_code;
            return ERROR;
        }

        if ((stream = fopen(filename, "r")))
        {
            if (key == STANDART && (error_code = get_structures(stream , full_one, vector)) != OK)
            {
                return ERROR;
            }
            else if (key == COORDINATE && (error_code = get_coordinates(stream , full_one, vector)) != OK)
            {
                return ERROR; 
            }

            fclose(stream);
        }
        else
        {
            errno = FILE_OPEN_ERROR;
            return ERROR;
        }
        
    }
    else
    {
        if (key == STANDART && (error_code = get_structures(stdin, full_one, vector)) != OK)
        {
            return ERROR;
        }
        else if (key == COORDINATE && (error_code = get_coordinates(stdin , full_one, vector)) != OK)
        {
            return ERROR; 
        }
    }

    return OK;
}

int translate_to_sparse(const full_matrix full_one, sparse_matrix *const sparse_one)
{
    sparse_one -> rows = full_one.rows;
    sparse_one -> cols = full_one.cols;

    int rows_counter = 0;

    for (int i = 0; i < full_one.rows; i++)
    {
        sparse_one -> rows_start[i] = rows_counter;

        for (int j = 0; j < full_one.cols; j++)
        {
            if (*(*(full_one.matrix + i) + j))
            {
                sparse_one -> values[rows_counter] = *(*(full_one.matrix + i) + j);
                sparse_one -> cols_number[rows_counter] = j;
                rows_counter++;
            }
        }
    }

    sparse_one -> rows_start[full_one.rows] = rows_counter;

    return OK;
}

int input_random(full_matrix *const full_one, full_matrix *const vector)
{
    int percent = 0, elems_to_fill = 0, counter = 0, index_i = 0, index_j = 0;

    printf("Введите кол-во строк матрицы (не более %d):\n", MAX_SIZE);
    if (scanf("%d", &(full_one -> rows)) != 1 || full_one -> rows < 0 || full_one -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    printf("Введите кол-во столбцов матрицы (не более %d):\n", MAX_SIZE);
    if (scanf("%d", &(full_one -> cols)) != 1 || full_one -> cols < 0 || full_one -> cols > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }

    fill_zero(full_one);

    printf("Введите процент заполнения матрицы(0 - 100):\n");

    if (scanf("%d", &percent) != 1 || percent < 0 || percent > 100)
    {
        errno = WRONG_PERCENT;
        return ERROR;;
    }

    elems_to_fill = (int) (full_one -> rows * full_one -> cols * (float) (percent) / 100);

    while (counter != elems_to_fill)
    {
        index_i = rand() % full_one -> rows;
        index_j = rand() % full_one -> cols;

        if (full_one -> matrix[index_i][index_j] == 0)
        {
            full_one -> matrix[index_i][index_j] = rand() % 10000;
            counter++;
        }
    }

    printf("Введите размерность вектора(не более %d)\n", MAX_SIZE);
    if (scanf("%d", &(vector -> rows)) != 1 || vector -> rows < 0 || vector -> rows > MAX_SIZE)
    {
        errno = WRONG_MATRIX_DIM;
        return ERROR;
    }
    
    vector -> cols = 1;

    printf("Введите процент заполнения ветора-столбца(0 - 100):\n");

    if (scanf("%d", &percent) != 1 || percent < 0 || percent > 100)
    {
        errno = WRONG_PERCENT;
        return ERROR;
    }

    fill_zero(vector);

    counter = 0;
    elems_to_fill = (int) (vector -> rows * vector -> cols * (float) (percent) / 100);
    
    while (counter != elems_to_fill)
    {
        index_i = rand() % vector -> rows;
        index_j = rand() % vector -> cols;

        if (vector -> matrix[index_i][index_j] == 0)
        {
            vector -> matrix[index_i][index_j] = rand() % 10000;
            counter++;
        }
    }

    return OK;
}