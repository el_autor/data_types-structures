#ifndef _SHOW_H_

#define _SHOW_H_

#include "structs.h"

void show_in_sparse(sparse_matrix sparse_one);
void show_full_standart(const full_matrix full_one);
void show_full_coordinate(const full_matrix full_one);
void show_sparse_standart(const sparse_matrix sparse_one);
void show_sparse_coordinate(const sparse_matrix sparse_one);
void show_stats(const full_matrix *const full_one,\
                const full_matrix *const vector,\
                const full_matrix *const result_one,\
                const sparse_matrix *const sparse_one,\
                const sparse_matrix *const sparse_vector,\
                const long double time_full,\
                const long double time_sparse);

#endif