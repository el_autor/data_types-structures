#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include "structs.h"
#include "input.h"
#include "errors.h"
#include "calculations.h"
#include "show.h"

#define OK 0
#define START_UP 1
#define STOP 0

#define INPUT_STANDART 1
#define INPUT_COORDINATE 2
#define INPUT_RANDOM 3
#define SHOW_INPUTED_STANDART 4
#define SHOW_INPUTED_COORDINATE 5
#define CALCULATE 6
#define FULL_OUTPUT_STANDART 7
#define FULL_OUTPUT_COORDINATE 8
#define SPARSE_OUTPUT_STANDART 9
#define SPARSE_OUTPUT_COORDINATE 10
#define SHOW_STATS 11
#define SHOW_IN_SPARSE 12
#define EXIT 13

void greeting()
{
    printf("        Программа для работы с разреженными матрицами\n");
    printf("\
Осуществляет умножение матрицы на вектор-столбец обычным методом и методом CSR, а\n\
а также показывает статистику по затраченному времени и памяти для обоиих методов\n\n");
}

int show_error(const int error_code)
{
    switch (error_code)
    {
        case WRONG_INPUT_CODE:
            printf("Ошибочный ввод кода!\n");
            break;
    
        case STRING_INPUT_OVERFLOW:
            printf("Введенная строка длиннее чем возможно!\n");
            break;

        case FILE_OPEN_ERROR:
            printf("Ошибка открытия файла!\n");
            break;

        case WRONG_MULTIPLY_DIMS:
            printf("Кол-во столбцов матрицы не совпадает с кол-вом строк вектора-столбца!\n");
            break;

        case WRONG_ELEM_INPUT:
            printf("Неверно введены элементы матрицы!\n");
            break;

        case WRONG_VECTOR_INPUT:
            printf("Неверно введены элементы вектора-столбца!\n");
            break;

        case WRONG_MATRIX_DIM:
            printf("Неверная размерность матрицы!\n");
            break;

        case INPUT_FIRST:
            printf("Сначала произведите ввод матрицы!\n");
            break;

        case CALCULATE_FIRST:
            printf("Сначала произведите расчет!\n");
            break;

        case INDEX_OVERFLOW:
            printf("Индекс за пределами размерности!\n");
            break;

        case WRONG_PERCENT:
            printf("Неверный процент заполения!\n");
            break;
    }

    return error_code;
}

void show_menu()
{
    printf("Выберите действие:\n\
1 - ввести матрицу в обычном формате\n\
2 - ввести матрицу по координатам\n\
3 - сгенерировать случайную матрицу\n\
4 - показать матрицу в обычном формате\n\
5 - показать матрицу в координатном формате\n\
6 - произвести вычисления\n\
7 - вывести результат ддя полной матрицы в стандартном виде\n\
8 - вывести результат для полной матрицы в координатном виде\n\
9 - вывести результат для разреженной матрицы в стандартном виде\n\
10 - вывести результат для разреженной матрицы в координатном виду\n\
11 - вывести затраченное время и память\n\
12 - вывести разреженный вид\n\
13 - выйти из программы\n");

}

int main()
{
    sparse_matrix sparse_one, sparse_vector, sparse_result;
    full_matrix full_one, full_vector, full_result;
    long double time_full = 0, time_sparse = 0; 
    int error_code = OK, action_code, process = START_UP;
    bool input_flag = false, calculate_flag = false;

    greeting();

    while (process)
    {
        show_menu();
        
        if (scanf("%d", &action_code) != 1 || action_code < 1 || action_code > 13)
        {
            show_error(WRONG_INPUT_CODE);
        }

        switch (action_code)
        {
            case EXIT:
                process = STOP;
                break;
        
            case INPUT_STANDART:
                if ((error_code = input_standart(&full_one, &full_vector, STANDART)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_one, &sparse_one)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_vector, &sparse_vector)))
                {
                    return show_error(errno);
                }

                input_flag = true;
                calculate_flag = false;
                break;

            case INPUT_COORDINATE:
                if ((error_code = input_standart(&full_one, &full_vector, COORDINATE)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_one, &sparse_one)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_vector, &sparse_vector)))
                {
                    return show_error(errno);
                }

                input_flag = true;
                calculate_flag = false;
                break;

            case INPUT_RANDOM:
                if ((error_code = input_random(&full_one, &full_vector)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_one, &sparse_one)))
                {
                    return show_error(errno);
                }

                if ((error_code = translate_to_sparse(full_vector, &sparse_vector)))
                {
                    return show_error(errno);
                }

                input_flag = true;
                calculate_flag = false;
                break;

            case SHOW_INPUTED_STANDART:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                
                show_full_standart(full_one);
                show_full_standart(full_vector);
                break;

            case SHOW_INPUTED_COORDINATE:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                
                show_full_coordinate(full_one);
                show_full_coordinate(full_vector);

                break;

            case CALCULATE:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }

                calculate_flag = true;

                if ((error_code = multiply_full(&full_result, full_one, full_vector, &time_full)))
                {
                    return show_error(errno);
                }

                if ((error_code = multiply_sparse(&sparse_result, sparse_one, sparse_vector, &time_sparse)))
                {
                    return show_error(errno);
                }

                break;

            case FULL_OUTPUT_STANDART:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_full_standart(full_result);

                break;

            case FULL_OUTPUT_COORDINATE:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_full_coordinate(full_result);
                break;

            case SPARSE_OUTPUT_STANDART:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_sparse_standart(sparse_result);
                break;

            case SPARSE_OUTPUT_COORDINATE:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_sparse_coordinate(sparse_result);
                break;
            
            case SHOW_IN_SPARSE:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_in_sparse(sparse_result);
                break;

            case SHOW_STATS:
                if (!input_flag)
                {
                    show_error(INPUT_FIRST);
                    continue;
                }
                else if (!calculate_flag)
                {
                    show_error(CALCULATE_FIRST);
                    continue;
                }

                show_stats(&full_one, &full_vector, &full_result, &sparse_one, &sparse_vector, time_full, time_sparse);
                break;
        }
    }

    return error_code;
}