#ifndef _CALCULATIONS_H_

#define _CALCULATIONS_H_

#include "structs.h"

int multiply_sparse(sparse_matrix *const result, const sparse_matrix sparse_one, const sparse_matrix sparse_vector, long double *const time);
int multiply_full(full_matrix *const result, const full_matrix full_one, const full_matrix full_vector, long double *const time);

#endif