#include <stdio.h>
#include <stdlib.h>

#define NORMAL 1
#define ERROR 0

#define TRUE 1
#define FALSE 0

#define DATA_SIZE 38 // Кол-во ячеек под хранение действительного числа
#define MANTISSA_SIZE 30 // Ячейки под хранение мантиссы
#define EXP_SIZE 6 // Кол-во ячеек под хранение порядка
#define MANTISSA_SIGN 0 // Номер ячейки со знаком мантиссы
#define EXP_SIGN 31 // Номер ячейки со знаком порядка

// Коды возврата
#define OK 0
#define REFILLING 1
#define INPUT_ERROR 2

// Токены функции move_to_right
#define MANTISSA 1
#define EXP 2

struct number // Структура для представления числа 
{
    char data[DATA_SIZE]; // Массив для хранения действителного числа
    int after_point; // Кол-во символов после точки
    int total_cells; // Обще кол-во символов
    char exponential_input; // Признак экпоненциального ввода
};

char greeting() // Приветствие пользователя
{
    char symb;
    printf("%s", "Программа моделирования операции умножения действительных чисел\n");
    printf("%s", "Нажмите Enter, чтобы продолжить или любую другую клавишу, чтобы выйти:\n");
    scanf("%c", &symb);
    if (symb == '\n')
    {
        printf("Введите 2 действительных числа в одном из двух форматов:\n \
    1) Десятичный: +-m.n, где m + n (кол-во значащих цифр) <= 30\n \
    2) Экспоненциальный: +-m.n E +-K, гдe m + n <= 30, а величина порядка K - не более 5 цифр\n \
    Результат выводится в формате: (+-)0.m1 E (+-)K1, где m1 - до 30 значащих цифр, а K1 - до 5 цифр\n");
        return NORMAL;
    }
    return ERROR;
}

void move_to_right(char *arr, char cell_counter, char token) // Выравнивание по правому краю в поле мантисссы
{
    if (token == MANTISSA)
    {
        int last_index = MANTISSA_SIZE;
        for (int i = cell_counter - 1; i > 0; i--)
        {
            arr[last_index] = arr[i];
            last_index--;
        }
        for (int i = 1; i <= MANTISSA_SIZE - cell_counter + 1; i++) // Зануление левого края
            arr[i] = 0;
    }
    else
    {
        int last_index = DATA_SIZE - 1;
        for (int i = cell_counter + MANTISSA_SIZE; i > MANTISSA_SIZE + 1; i--) // Выравнивание по правому краю в поле порядка
        {
            arr[last_index] = arr[i];
            last_index--;
        }
        for (int i = MANTISSA_SIZE + 2; i < MANTISSA_SIZE + EXP_SIZE - cell_counter + 3; i++)
            arr[i] = 0;
    }
}

char input(struct number *numeric) // Функция, проверяющая правильность ввода действительного числа
{
    char symb, point_flag = FALSE, space_flag = FALSE,
         start_space_flag = TRUE, sign_able = FALSE, sign_able_exp = FALSE;
    int cell_counter = 1, exp_cell_counter = 1;

    numeric -> after_point = 0;
    numeric -> exponential_input = FALSE;
    numeric -> data[MANTISSA_SIGN] = '+'; // Знак числа

    numeric->data[EXP_SIGN] = '+';
    for (int i = EXP_SIGN + 1; i < DATA_SIZE; i++) // Инициализация порядка
        numeric -> data[i] = 0; 

    while ((symb = getchar()) != '\n')
    {
        if (start_space_flag)
        {
            while (symb == ' ')
                symb = getchar();
            start_space_flag = FALSE;
        }
        if (!numeric -> exponential_input) // Если еще идет ввод мантиссы
        {  
            if ((symb == '-' || symb == '+') && (cell_counter > 1 || sign_able))
                return ERROR;    
            else if (symb == '-' || symb == '+')
            {
                sign_able = TRUE;
                if (symb == '-')
                    numeric->data[0] = symb;
            }
            else if (symb == ' ' && cell_counter > 1)
                space_flag = TRUE;
            else if (symb == '.' && point_flag == TRUE) // Ошибка, если встретилась 2 точка
                return ERROR;
            else if (symb == '.')
                point_flag = TRUE;
            else if (symb == 'E' && cell_counter > 1) // Условие начала ввода порядка
            {
                space_flag = FALSE;
                numeric -> exponential_input = TRUE; 
                numeric -> total_cells = cell_counter - 1; 
            }
            else if ((symb < 48 || symb > 57) && symb != ' ') // Ошибка, если во вводе попалась буква
                return ERROR;
            else if (cell_counter == 31 || space_flag == TRUE) // Ошибка, если введенных знаков больше
                return ERROR;
            else if (point_flag)
            {
                numeric -> data[cell_counter++] = symb - 48;
                numeric -> after_point++;
            }
            else if (symb >= 48 || symb <= 57)
                numeric -> data[cell_counter++] = symb - 48;
        }
        else
            
            if (symb == '-' || symb == '+')
            {
                if (sign_able_exp)
                {
                    return ERROR;
                }

                sign_able_exp = TRUE;
                if (symb == '-')
                    numeric -> data[EXP_SIGN] = '-';
            }
            else if (symb == ' ' && exp_cell_counter > 1)
                space_flag = TRUE;
            else if ((symb < 48 || symb > 57) && symb != ' ')
                return ERROR;
            else if (exp_cell_counter > 5 || space_flag == TRUE)
                return ERROR;
            else if (symb >= 48 && symb <= 57)
            {
                numeric -> data[EXP_SIGN + exp_cell_counter] = symb - 48;
                exp_cell_counter++;
            }
    }
    if (symb == '\n' && cell_counter == 1)
    {
        return ERROR;
    }

    numeric -> total_cells = cell_counter - 1;
    move_to_right(numeric -> data, cell_counter, MANTISSA); // Выравнивание мантиссы числа в памяти по правому краю
    if (numeric -> exponential_input == TRUE)
    {
        move_to_right(numeric -> data, exp_cell_counter, EXP);
    }

    return NORMAL;
}

void delete_last_zeros(char *arr, int index)
{
    int times = 0;
    while (!arr[index] && times < index)
    {
        times++;

        for (int i = index; i > 1; i--)
        {
            arr[i] = arr[i - 1];
        }
    }
}

int get_int_from_chars(char *arr) // Получение числа по массиву символов
{
    int exponent = 0, start = 1;
    for (int i = DATA_SIZE - 1; i >= EXP_SIGN + 1; i--)
    {
        exponent += start * arr[i];    
        start *= 10;
    }
    if (arr[EXP_SIGN] == '-')
        exponent *= -1;
    return exponent;
}

void change_exp(char *arr, int delta) // Изменение порядка
{
    int exponent = get_int_from_chars(arr) + delta;
    arr[EXP_SIGN] = (exponent < 0) ? '-' : '+';
    exponent = abs(exponent);
    for (int i = DATA_SIZE - 1; i >= EXP_SIGN + 1; i--)
    {
        arr[i] = exponent % 10;
        exponent /= 10;
    }
}
     
void normilize(struct number *numeric) // Приведение числа с нормализованному виду
{
    //printf("%d - total_cells\n", numeric -> total_cells);
    //printf("%d - after point\n", numeric -> after_point);
    if (numeric -> data[MANTISSA_SIZE - numeric -> total_cells + 1] != 0)
    {
        //printf("%d\n", numeric -> total_cells - numeric -> after_point);
        change_exp(numeric -> data, numeric -> total_cells - numeric -> after_point);
        numeric -> after_point = numeric -> total_cells;
    }
    else if (numeric -> total_cells != 1)
    {
        while (numeric -> data[MANTISSA_SIZE - numeric -> total_cells + 1] == 0 && numeric -> total_cells > 1)
        {
            numeric -> total_cells--;
        }
        
        normilize(numeric);
    }
    if (numeric -> after_point == 0)
    {
        int index = 0;
        for (int i = MANTISSA_SIZE - numeric -> after_point + 1; !numeric -> data[i]; i++)
            index--;
        change_exp(numeric -> data, index);
    }
    delete_last_zeros(numeric -> data, MANTISSA_SIZE);
}

void print(char *data) // Отладочный вывод введенных чисел
{
    for (int i = 0; i < DATA_SIZE; i++)
    {
        if (i == EXP_SIGN || i == MANTISSA_SIGN)
            printf("%c ", data[i]);      
        else    
            printf("%d ", data[i]);
    }
    printf("\n");    
}

void zero_arr(char *arr) // Обнуление масссива - буфера
{
    for (int i = 0; i <= MANTISSA_SIZE; i++)
        arr[i] = 0;
}

void add_buffer_to_result(char *result, char *buf, int head, int tail) // Добавление массива-буфера к результату
{
    int buffer = 0;
    for (int i = tail, j = MANTISSA_SIZE; i >= head; i--, j--)
    {
        int local = result[i];
        result[i] = (result[i] + buf[j] + buffer) % 10;
        buffer = (local + buf[j] + buffer) / 10;
    }
    if (buf[0] || buffer)
        result[head - 1] += buf[0] + buffer;
}

char calculate(struct number number_1, struct number number_2, char *result) // Расчет результата
{
    int common_exp = get_int_from_chars(number_1.data) + get_int_from_chars(number_2.data);
    result[2 * MANTISSA_SIZE + 1] = (common_exp < 0) ? '-' : '+';

    for (int i = 2 * MANTISSA_SIZE + 2; i < 2 * MANTISSA_SIZE + 2 + EXP_SIZE; i++) // Обнуление полей порядка
        result[i] = 0;
    common_exp = abs(common_exp);
    for (int i = 2 * MANTISSA_SIZE + EXP_SIZE + 1; i >= 2 * MANTISSA_SIZE + 2; i--)
    {          
        result[i] = common_exp % 10;
        common_exp /= 10;
    }

    char buffer_array[MANTISSA_SIZE + 1]; // Массив - буфер
    if ((number_1.data[MANTISSA_SIGN] == '-' && number_2.data[MANTISSA_SIGN] == '-') || // Задание знака результата
        (number_1.data[MANTISSA_SIGN] == '+' && number_2.data[MANTISSA_SIGN] == '+'))
        result[MANTISSA_SIGN] = '+';
    else
        result[MANTISSA_SIGN] = '-';
    for (int i = 1; i <= 2 * MANTISSA_SIZE ; i++) // Обнуление мантиссы результирующего массива
        result[i] = 0;
    int head = MANTISSA_SIZE + 1, tail = MANTISSA_SIZE * 2; // Начальный диапазон для сложения
    for (int i = MANTISSA_SIZE; i >= 1; i--)
    {
        zero_arr(buffer_array);
        int buffer = 0;
        for (int j = MANTISSA_SIZE; j >= 1; j--)
        {
            buffer_array[j] = (number_2.data[i] * number_1.data[j] + buffer) % 10;
            buffer = (number_2.data[i] * number_1.data[j] + buffer) / 10;
        }
        if (buffer)
            buffer_array[MANTISSA_SIGN] = buffer;
        add_buffer_to_result(result, buffer_array, head, tail);
        head--;
        tail--;
    }
    return NORMAL;
}

void round_if_need(int after_point, char *result) // Округление
{
    if (after_point > 29)
    {
        int from_back = 2 * MANTISSA_SIZE - (after_point - 29) + 1;
        if (result[from_back] >= 5)
        {
            while (result[--from_back] == 9)
                result[from_back] = 0;
            result[from_back]++;
        }
    }
}   

int get_grade(char *result)
{
    int grade = 0, mult = 1;

    for (int i = 2 * MANTISSA_SIZE + 1 + EXP_SIZE; i > 2 * MANTISSA_SIZE + 1; i--)
    {
        grade += mult * result[i];
        mult *= 10;
    }

    if (result[2 * MANTISSA_SIZE + 1] == '-')
    {
        grade *= -1;
    }
    return grade;
}

void minus_grade(char *result) // Понижение порядка на 1
{
    int grade = get_grade(result);

    grade--;

    if (grade < 0)
    {
        result[2 * MANTISSA_SIZE + 1] = '-';
        grade *= -1;
    }

    int index = 2 * MANTISSA_SIZE + 1 + EXP_SIZE;
    while (grade)
    {
        result[index] = grade % 10;
        grade /= 10;
        index--; 
    }

    while (index != 2 * MANTISSA_SIZE + 1)
    {
        result[index] = 0;
        index--;
    }    
}

int output_result(int after_point, char *result) // Вывод конечного результатта в заданном формате
{
    int flag = FALSE, counter = 1, wall;
    
    /*
    for (int i = 0; i < 2 * MANTISSA_SIZE + 2 + EXP_SIZE; i++)
    {
        if (i == 0 || i == 61)
            printf("%c ", result[i]);
        else
            printf("%d ", result[i]);
    }
    printf("\n");*/
    

    round_if_need(after_point - 1, result);
    
    //printf("%d point\n", after_point);
    while(!result[2 * MANTISSA_SIZE - after_point + 1])
    {
        after_point--;
        minus_grade(result);
    }

    if (abs(get_grade(result)) > 99999)
    {
        return ERROR;
    }

    if (result[MANTISSA_SIGN] == '-')
        printf("-");
    printf("0.");

    int start;
    if (after_point > 29)
    {
        if (2 * MANTISSA_SIZE - after_point + 1 == 0)
        {
            wall = 2 * MANTISSA_SIZE - (after_point - 29) + 1;
            start = 1;
        }
        else
        {
            wall = 2 * MANTISSA_SIZE - (after_point - 29);
            start = 2 * MANTISSA_SIZE - after_point + 1;
        }
        while (!result[wall])
            wall--;
    }
    else
    {
        wall = 2 * MANTISSA_SIZE;
        start = 2 * MANTISSA_SIZE - after_point + 1;
    }

    for (int i = start; i <= wall; i++)
        if (counter <= MANTISSA_SIZE)
        {
            counter++;
            printf("%d", result[i]);
        }

    printf("E");

    if (result[2 * MANTISSA_SIZE + 1] == '-')
        printf("-");
    else
        printf("+");
    for (int i = 2 * MANTISSA_SIZE + 2; i < 2 * MANTISSA_SIZE + 2 + EXP_SIZE; i++)
        if ((result[i] || flag))
        {
            flag = TRUE;
            printf("%d", result[i]);
        }
    if (!flag)
        printf("0");
    return NORMAL;
}

int get_valuable_last(char *data, int index)
{
    int buffer = 0, value_after_point = 0;
    for (int i = index; i > 0; i--)
    {
        if (data[i])
        {
            value_after_point += buffer + 1;
            buffer = 0;
        }
        else
        {
            buffer++;
        }
    }
    return value_after_point;
}

int main()  
{
    struct number number_1, number_2; 
    char result[2 * MANTISSA_SIZE + 2 + EXP_SIZE];
    char value_after_point = 0;

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);

    if (!(greeting()))
    {
        return OK;
    }

    if (input(&number_1) && input(&number_2))
    {
        
        //print(number_1.data);
        //print(number_2.data);
        

        normilize(&number_1);
        normilize(&number_2);

        /*
        print(number_1.data);
        print(number_2.data);
        printf("%d\n", number_1.after_point);
        printf("%d\n", number_2.after_point);
        */

        if (calculate(number_1, number_2, result))
        {
            int buffer = 0;
            for (int i = 2 * MANTISSA_SIZE; i > 0; i--)
            {
                if (result[i])
                {
                    value_after_point += buffer + 1;
                    buffer = 0;
                }
                else
                {
                    buffer++;
                }
            }
            value_after_point = get_valuable_last(result, 2 * MANTISSA_SIZE);
            //printf("%d\n", value_after_point);
            //printf("%d\n", get_valuable_last(number_1.data, MANTISSA_SIZE) + get_valuable_last(number_2.data, MANTISSA_SIZE));
            if (value_after_point == get_valuable_last(number_1.data, MANTISSA_SIZE) + get_valuable_last(number_2.data, MANTISSA_SIZE))
            {
                if (!output_result(value_after_point, result))
                {
                    printf("Переполнение порядка");
                    return REFILLING;
                }
            }
            else
            {
                if (!output_result(value_after_point + 1, result))
                {
                    printf("Переполнение порядка");
                    return REFILLING;
                }
            }  
        }
    }
    else
    {
        printf("Неверный формат ввода");
        return INPUT_ERROR;
    }

    return OK;
}